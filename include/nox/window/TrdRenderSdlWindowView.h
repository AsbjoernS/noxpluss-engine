
#ifndef NOX_WINDOW_TRDRENDERSDLWINDOWVIEW_H_
#define NOX_WINDOW_TRDRENDERSDLWINDOWVIEW_H_

#include "SdlWindowView.h"
#include <nox/app/log/Logger.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>

namespace nox
{

namespace app { namespace graphics
{

	class ITrdRenderer;

} }

namespace window
{

class TrdRenderSdlWindowView: public SdlWindowView, public logic::event::IListener
{
public:
	TrdRenderSdlWindowView(app::IContext* applicationContext, const std::string& windowTitle);
	virtual ~TrdRenderSdlWindowView();

	void render();

	virtual void onSdlEvent(const SDL_Event& event) override;

protected:
	logic::IContext* getLogicContext();
	app::IContext* getApplicationContext();

	virtual bool initialize(logic::IContext* context) override;
	virtual void onWindowSizeChanged(const glm::uvec2& size) override;
	virtual void onEvent(const std::shared_ptr<logic::event::Event>& event) override;
	virtual void onDestroy() override;
private:
	virtual void onRendererCreated(app::graphics::ITrdRenderer* renderer) = 0;

	bool onWindowCreated(SDL_Window* window) override final;

	app::IContext* applicationContext;
	logic::IContext* logicContext;
	app::log::Logger log;
	SDL_Window* window;

	std::unique_ptr<app::graphics::ITrdRenderer> renderer;

	logic::event::ListenerManager listener;
};

}
}

#endif
