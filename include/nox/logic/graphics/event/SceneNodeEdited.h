#ifndef NOX_LOGIC_GRAPHICS_SCENENODEEDITED_H_
#define NOX_LOGIC_GRAPHICS_SCENENODEEDITED_H_

#include <nox/logic/event/Event.h>
#include <nox/app/graphics/SceneGraphNode.h>

namespace nox { namespace logic { namespace graphics
{

/**
 * A scene graph node was either added or removed.
 */
class SceneNodeEdited: public event::Event
{
public:
	enum class Action
	{
		CREATE,
		REMOVE
	};

	static IdType ID;

	SceneNodeEdited(const std::shared_ptr<app::graphics::SceneGraphNode>& sceneNode, Action action);

	const std::shared_ptr<app::graphics::SceneGraphNode>& getSceneNode() const;
	Action getEditAction() const;

private:
	std::shared_ptr<app::graphics::SceneGraphNode> sceneNode;
	Action action;
};

} } }

#endif /* SCENENODECREATEEVENT_H_ */
