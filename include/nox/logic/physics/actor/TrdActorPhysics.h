/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_TRDACTORPHYSICS_H_
#define NOX_LOGIC_PHYSICS_TRDACTORPHYSICS_H_

#include "../physics_utils.h"
#include <glm/vec2.hpp>
#include <nox/logic/actor/Component.h>
#include <nox/util/math/Box.h>
#include <nox/logic/physics/TrdSimulation.h>

namespace nox { namespace logic
{

namespace actor
{

class Identifier;

}

namespace physics
{


/**
 * Enables an Actor to be physically simulated.
 *
 * # Dependencies
 * - Transform
 *
 * # JSON Description
 * ## Name
 * %Physics
 *
 * ## Properties
 *
 */
class TrdActorPhysics: public actor::Component
{
public:
	static const IdType NAME;

	const IdType& getName() const override;
	bool initialize(const Json::Value& componentJsonObject) override;
	void onCreate() override;
	void onDestroy() override;
	void onComponentEvent(const std::shared_ptr<event::Event>& event) override;
	void onUpdate(const Duration& deltaTime) override;
	void onActivate() override;
	void onDeactivate() override;

private:
	void serialize(Json::Value& componentObject) override;

	BulletBodyDefinition physicalProperties;

	TrdSimulation* physics;
	
};

} } }

#endif
