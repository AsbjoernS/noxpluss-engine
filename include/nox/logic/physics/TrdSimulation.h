/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_ITRDSIMULATION_H_
#define NOX_LOGIC_PHYSICS_ITRDSIMULATION_H_

#include "physics_utils.h"
#include "joint.h"
#include <nox/logic/event/Event.h>
#include <nox/app/graphics/GeometrySet.h>
#include <nox/util/Mask.h>
#include <nox/common/types.h>
#include <memory>
#include <glm/glm.hpp>
#include <nox/util/boost_utils.h>
#include <vector>
#include <unordered_set>
#include <functional>

namespace nox
{

namespace app
{
namespace graphics
{


}
}

namespace logic
{

namespace actor
{

class Actor;

}

namespace physics
{

class TrdSimulation
{
public:

	virtual ~TrdSimulation();

	/**
	 *  Handles the updating of the world.
	 *  By stepping the physics using the deltaTime.
	 *  @param deltaTime The time since the last update.
	 */
	virtual void onUpdate(const Duration& deltaTime) = 0;

	virtual void createActorBody(actor::Actor* actor, const BulletBodyDefinition& bodyDefinition) = 0;

	virtual bool getDebugRenderingEnabled() = 0;
	
};

} } }

#endif
