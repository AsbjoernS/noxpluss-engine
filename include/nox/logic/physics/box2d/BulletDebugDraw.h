/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_BULLETDEBUGDRAWER_H_
#define NOX_LOGIC_PHYSICS_BULLETDEBUGDRAWER_H_

#include <nox/app/graphics/opengl/RenderData.h>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

#include <btBulletDynamicsCommon.h>
#include <vector>

namespace nox { namespace logic { namespace physics
{

class BulletDebugDraw : public btIDebugDraw
{
public:
	BulletDebugDraw();
	virtual ~BulletDebugDraw();

	void drawLine(const btVector3& from, const btVector3& to, const btVector3& color) override;
	void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color) override;
	void reportErrorWarning(const char* warningString) override;
	void draw3dText(const btVector3& location, const char* textString) override;
	void setDebugMode(int debugMode) override;
	int getDebugMode() const override;

	void onDebugRender(nox::app::graphics::RenderData renderData, const glm::mat4x4& viewPorjection);
	void toggleDebugDraw();

	bool getIsDebugModeEnabled();

protected:

private:
	GLuint debugVAO;
	GLuint debugVBO;

	int mvpUniform;
	int debugMode;

	bool VAOInited;
	bool isDebugModeEnabled;

	std::vector<glm::vec3> vertices;
};

} } }

#endif
