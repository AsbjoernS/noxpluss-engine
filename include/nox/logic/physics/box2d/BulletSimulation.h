/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_BULLETSIMULATION_H_
#define NOX_LOGIC_PHYSICS_BULLETSIMULATION_H_

#include <nox/app/log/Logger.h>
#include <nox/app/graphics/GeometrySet.h>
#include <nox/logic/actor/Identifier.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/physics/TrdSimulation.h>
#include <nox/util/Booker.h>
#include <nox/util/math/Box.h>
#include <nox/util/thread/ThreadBarrier.h>
#include <nox/logic/actor/Actor.h>

#include <memory>
#include <unordered_map>
#include <set>
#include <queue>
#include <mutex>
#include <thread>
#include <functional>
#include <glm/glm.hpp>
#include <Box2D/Box2D.h>
#include <nox/logic/event/Event.h>

#include <btBulletDynamicsCommon.h>

#include <nox/util/chrono_utils.h>
#include <nox/util/math/Box.h>

#include <nox/logic/physics/box2d/BulletDebugDraw.h>

namespace nox
{

namespace app
{
namespace graphics
{

class Light;

}
}

namespace logic
{

class IContext;

namespace physics
{

/**
 *  Handles physics simulation using the Bullet engine.
 */

class BulletSimulation : public TrdSimulation, public event::IListener
{
public:
	/**
	 *  Initiates the Bullet simulator.
	 */
	BulletSimulation(IContext* logicContext);

	virtual ~BulletSimulation();

	void setLogger(app::log::Logger logger);

	void onUpdate(const Duration& deltaTime) override;
	void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;
	btCollisionShape* createShape(const BulletBodyDefinition& bodyDefinition);
	static void myTickCallback(btDynamicsWorld* world, btScalar timeStep);
	void createActorBody(actor::Actor* actor, const BulletBodyDefinition& bodyDefinition) override;

	bool getDebugRenderingEnabled() override;

	void setDebugRenderer(BulletDebugDraw* debugRenderer);

	static glm::vec3 btVector3_to_Vec3(btVector3 const & btvec);

protected:

private:
	struct TrdActorData
	{
		TrdActorData() :
			actor(nullptr),
			mainBody(nullptr)
		{}
		actor::Actor* actor;
		btRigidBody* mainBody;
	};

	mutable app::log::Logger log;

	IContext* logicContext;
	event::ListenerManager listener;



	typedef std::pair< const btRigidBody * , const btRigidBody  * > CollisionPair;
	typedef std::set< CollisionPair > CollisionPairs;

	CollisionPairs oldTickCollisionPairs;
	


	void sendCollisionPairAddEvent(const btPersistentManifold* manifold, const btRigidBody* bodyOne, const btRigidBody* bodyTwo);
	void sendCollisionPairRemoveEvent(const btRigidBody* bodyOne, const btRigidBody* bodyTwo);


	// TODO: Create data structure for body
	std::unordered_map<actor::Identifier, TrdActorData> actorMap;
	
	std::shared_ptr<btDbvtBroadphase> broadphase;
	std::shared_ptr<btDefaultCollisionConfiguration> collisionConfiguration;
	std::shared_ptr<btCollisionDispatcher> dispatcher;
	std::shared_ptr<btSequentialImpulseConstraintSolver> solver;
	std::shared_ptr<btDiscreteDynamicsWorld> dynamicsWorld;

	BulletDebugDraw* debugRenderer;

};
} } }

#endif
