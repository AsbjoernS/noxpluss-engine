#ifndef NOX_LOGIC_ACTOR_TRDTRANSFORMCHANGE_H_
#define NOX_LOGIC_ACTOR_TRDTRANSFORMCHANGE_H_

#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>
#include <nox/logic/actor/event/Event.h>


namespace nox {	namespace logic { namespace actor
{

class Actor;

/**
* The transformation of an actor changed.
*/
class TrdTransformChange final : public Event
{
public:
	static const event::Event::IdType ID;

	TrdTransformChange(Actor* transformedActor, const glm::vec3& position, const glm::vec3 rotation, const glm::vec3& scale);
	~TrdTransformChange();

	glm::mat4 getTransformMatrix() const;
	const glm::vec3& getPosition() const;
	const glm::vec3& getScale() const;
	const glm::vec3& getRotation() const;

private:
	glm::vec3 position;
	glm::vec3 scale;
	glm::vec3 rotation;
};

} } }

#endif