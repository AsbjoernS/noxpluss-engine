/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_ITRDRENDERER_H_
#define NOX_APP_GRAPHICS_ITRDRENDERER_H_

#include <memory>
#include <string>
#include <glm/vec2.hpp>


#include <nox/app/graphics/TrdSceneLoader.h>
#include <nox/app/graphics/TrdGraphicsAssetManager.h>
#include <nox/app/graphics/TrdTransformationNode.h>
#include <nox/logic/physics/box2d/BulletDebugDraw.h>

namespace nox { namespace app
{

class IContext;

namespace resource
{

class Descriptor;
class IResourceAccess;

}

namespace graphics
{

class TrdCamera;

class ITrdRenderer
{
public:
	virtual ~ITrdRenderer();
	virtual bool init(IContext* context, const std::string& shaderDirectory, const glm::uvec2& windowSize) = 0;
	virtual void onRender() = 0;
	virtual void setCamera(const std::shared_ptr<TrdCamera>& camera) = 0;
	virtual bool toggleDebugRendering() = 0;
	virtual bool isDebugRenderingEnabled() = 0;
	virtual void resizeWindow(const glm::uvec2& windowSize) = 0;

	virtual void setRootSceneNode(const std::shared_ptr<TrdTransformationNode>& rootSceneNode) = 0;
	virtual void setGraphicsAssetManager(const std::shared_ptr<TrdGraphicsAssetManager>& assetManager) = 0;

	virtual nox::logic::physics::BulletDebugDraw* getDebugRenderer() = 0;
};

}
} }

#endif
