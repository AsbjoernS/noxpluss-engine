/*
* NOX Engine
*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#ifndef NOX_APP_GRAPHICS_TRDCAMERA_H_
#define NOX_APP_GRAPHICS_TRDCAMERA_H_

#include <glm/glm.hpp>

namespace nox {
	namespace app
	{
		namespace graphics
		{

			class TrdCamera
			{
			public:

				TrdCamera(const glm::uvec2& size);

				glm::mat4x4& getViewProjectionMatrix();
				const glm::mat4x4& getViewMatrix() const;
				void updateViewMatrix();

				void setPosition(const glm::vec3 &position);
				glm::vec3 getPosition() const;

				void setScale(const glm::vec2& scale);
				glm::vec2 getScale() const;

				glm::vec2 getSize() const;
				void setSize(const glm::uvec2& size);

				void moveForward();
				void moveBackward();
				void moveLeft();
				void moveRight();
				void mouseUpdate(const int x,const int y);
				void reset();
				
			private:

				glm::uvec2 cameraSize;

				glm::mat4x4 viewProjectionMatrix;
				glm::mat4x4 viewMatrix;
				glm::mat4x4 projectionMatrix;

				glm::vec3 cameraPosition;
				glm::vec3 viewDirection;
				glm::vec3 upDirection;

				glm::vec2 cameraScale;
				glm::vec2 oldMousePosition;

				float zfar;			//How far from the camera you can see
				float znear;		//How close to the camera you can see
				float moveSpeed;	//Movement speed of camera
				float fov;			//field of view

				const int MOUSE_MOVE_MAX = 30;
				const float ROTATIONAL_SPEED = 0.01f;
			};

		}
	}
}

#endif
