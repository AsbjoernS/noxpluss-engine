/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_TRDTRANSFORMATIONNODE_H_
#define NOX_APP_GRAPHICS_TRDTRANSFORMATIONNODE_H_

#include "SceneGraphNode.h"
#include <nox/app/graphics/TrdGraphicsAssetManager.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/actor/component/TrdTransform.h>
#include <string.h>

namespace nox { 

namespace logic
{
	class IContext;
}

namespace app
{
namespace graphics
{

/**
 * Transforms the modelMatrix for it's children, restoring
 * the original when leaving.
 */
class TrdTransformationNode : public SceneGraphNode
{
public:
	TrdTransformationNode();
	TrdTransformationNode(const nox::logic::actor::TrdTransform* transformation, std::string name);
	~TrdTransformationNode();
    
	void setTransform(const glm::mat4& matrix);
	const glm::mat4& getTransform() const;
	

private:
	void onNodeEnter(TrdGraphicsAssetManager& assManager, glm::mat4x4& viewProjection, unsigned int mvpHandle, unsigned int programId) override;
    
	void onNodeLeave(TrdGraphicsAssetManager& assManager, unsigned int programId) override;


	const nox::logic::actor::TrdTransform* transformationComponent;
    
	glm::mat4x4 transformationMatrix;
	glm::mat4x4 savedMatrix;

	std::string name;

};

}
} }

#endif
