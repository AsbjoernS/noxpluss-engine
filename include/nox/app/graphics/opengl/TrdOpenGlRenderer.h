/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_TRDOPENGLRENDERER_H_
#define NOX_APP_GRAPHICS_TRDOPENGLRENDERER_H_

#include <GL/glew.h>
#include <vector>
#include <memory>
#include <chrono>

//#define TIME_BLUR //Used for timing the blur rendering.

#include <glm/glm.hpp>
#include <string>
#include <map>
#include <mutex>
#include <thread>

#include <nox/app/graphics/ITrdRenderer.h>
#include <nox/app/log/Logger.h>
#include <nox/util/thread/ThreadBarrier.h>
#include "../GeometrySet.h"
#include "TextureManager.h"
#include "opengl_utils.h"
#include "TextureRenderer.h"
#include "RenderData.h"
#include <nox/app/graphics/TrdTransformationNode.h>

#include <nox/logic/physics/box2d/BulletDebugDraw.h>

namespace nox { namespace app
{

namespace resource
{

class IResourceAccess;

}

namespace graphics
{

class SceneGraphNode;
class DebugRenderer;
class SubRenderer;

class TrdOpenGlRenderer : public ITrdRenderer
{
public:
	TrdOpenGlRenderer();

	~TrdOpenGlRenderer();

	bool init(IContext* context, const std::string& shaderDirectory, const glm::uvec2& windowSize) override;
	void onRender() override;
	void setCamera(const std::shared_ptr<TrdCamera>& camera) override;
	void resizeWindow(const glm::uvec2& windowSize) override;
	bool toggleDebugRendering() override;
	bool isDebugRenderingEnabled() override;
	void setRootSceneNode(const std::shared_ptr<TrdTransformationNode>& rootSceneNode) override;
	void setGraphicsAssetManager(const std::shared_ptr<TrdGraphicsAssetManager>& assetManager) override;

	nox::logic::physics::BulletDebugDraw* getDebugRenderer() override;

private:
	struct VertexAndFragmentShader
	{
		bool isValid() const;

		GLuint vertex = 0u;
		GLuint fragment = 0u;
	};

	struct StenciledTiledTextureLayer
	{
		StenciledTiledTextureLayer();
		~StenciledTiledTextureLayer();

		unsigned int renderLevel;
	};

	bool initOpenGL(const glm::uvec2& windowSize);

	GLuint createShader(resource::IResourceAccess* resourceCache, const std::string& shaderPath, const GLenum shaderType) const;
	VertexAndFragmentShader createVertexAndFragmentShader(resource::IResourceAccess* resourceCache, const std::string& shaderPath) const;
	
	mutable log::Logger log;

	std::shared_ptr<TrdGraphicsAssetManager> assetManager;
	std::shared_ptr<TrdCamera> renderCamera;

	//std::unordered_map<std::string, ShaderInfo> shaders;

	bool windowSizeChanged;
	glm::uvec2 windowSize;

	GLuint mvpHandle;
	GLuint lightPosHandle;
	GLuint cameraPosHandle;

	TrdSceneLoader* trdScene;
	GLuint simple3Dprogram;
	GLuint debugRenderProgram;

	RenderData renderData;
	std::unique_ptr<nox::logic::physics::BulletDebugDraw> debugRenderer;

	std::shared_ptr<TrdTransformationNode> rootSceneNode;
	
};

}
} }

#endif
