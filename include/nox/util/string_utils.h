/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_UTIL_STRINGUTILS_H_
#define NOX_UTIL_STRINGUTILS_H_

#include <nox/common/platform.h>

#include <string>
#include <sstream>
#include <vector>

namespace nox
{
namespace util
{

/**
 * Convert a value to a string.
 * Uses std::to_string if available, otherwise it streams the value to a stream.
 * @param value Value to convert.
 * @return String representation of the value.
 */
template<typename Type>
std::string toString(Type value);

/**
 * Split a string into tokens.
 * @param string String to split.
 * @param splitter Character to split the string on.
 * @tparam ContainerType The type of container that the tokens will be stored and returned in.
 * @return All of the tokens from the split string.
 */
template<template<class, class> class ContainerType = std::vector>
ContainerType<std::string, std::allocator<std::string>> splitString(const std::string& string, const char splitter);

/**
 * Match a string against a pattern.
 *
 * Asterisk (*) is a wildcard, while question mark (?) matches a single character.
 *
 * @param pattern Pattern to match against.
 * @param str String to match.
 * @return true if they match, otherwise false.
 */
bool matchWildcard(const char *pattern, const char *str);

/**
 * Remove all spaces from a string.
 * @param string String with spaces.
 * @return String without spaces.
 */
std::string removeSpaces(const std::string& string);


template<typename Type>
std::string toString(Type value)
{
#if NOX_OS_ANDROID
	std::ostringstream stringStream;
	stringStream << value;
	return stringStream.str();
#else
	return std::to_string(value);
#endif
}

template<template<class, class> class ContainerType>
ContainerType<std::string, std::allocator<std::string>> splitString(const std::string& string, const char splitter)
{
	ContainerType<std::string, std::allocator<std::string>> pathTokens;

	bool readingToken = false;
	std::string currentToken;

	auto pathIt = string.begin();
	while (pathIt != string.end())
	{
		if (*pathIt != splitter && readingToken == false)
		{
			readingToken = true;
		}
		else if (*pathIt == splitter && readingToken == true)
		{
			pathTokens.push_back(currentToken);
			currentToken.clear();
			readingToken = false;
		}

		if (readingToken == true)
		{
			currentToken += *pathIt;
		}

		pathIt++;
	}

	if (readingToken == true)
	{
		pathTokens.push_back(currentToken);
	}

	return pathTokens;
}

}
}

#endif
