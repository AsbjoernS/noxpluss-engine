/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/resource/Directory.h>

namespace nox { namespace app
{
namespace resource
{

Directory::Directory()
{
}

Directory::Directory(const std::string& name):
	name(name)
{
}

const std::string& Directory::getName() const
{
	return this->name;
}

const std::vector<Directory>& Directory::getSubdirectories() const
{
	return this->subirectories;
}

const std::vector<Descriptor>& Directory::getResources() const
{
	return this->resources;
}

void Directory::addSubdirectory(const Directory& subdirectory)
{
	this->subirectories.push_back(subdirectory);
}

void Directory::addResource(const Descriptor& resource)
{
	this->resources.push_back(resource);
}

}
} }
