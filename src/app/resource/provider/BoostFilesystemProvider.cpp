/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/resource/Descriptor.h>
#include <nox/app/resource/provider/BoostFilesystemProvider.h>

#include <fstream>
#include <cassert>

namespace nox { namespace app
{
namespace resource
{

BoostFilesystemProvider::BoostFilesystemProvider(std::string rootPathString):
	ignoreHiddenFiles(true)
{
	// Remove all trailing slashes.
	while (rootPathString.back() == '/' || rootPathString.back() == '\\')
	{
		rootPathString.erase(rootPathString.size() - 1, 1);
	}

	this->rootPath = rootPathString;
	this->rootPath.make_preferred();
}

void BoostFilesystemProvider::setLogger(log::Logger log)
{
	this->log = std::move(log);
	this->log.setName("BoostFilesystemProvider");
}

bool BoostFilesystemProvider::open()
{
	if (bfs::exists(this->rootPath) == false)
	{
		this->log.error().format("Failed opening provider: Root path \"%s\" does not exist.", this->rootPath.c_str());
		return false;
	}

	if (bfs::is_symlink(this->rootPath) == true)
	{
		this->rootPath = bfs::read_symlink(this->rootPath);
	}

	if (bfs::is_directory(this->rootPath) == false)
	{
		this->log.error().format("Failed opening provider: BoostFilesystemResourceProvider::open: Root path \"%s\" is not a directory.", this->rootPath.c_str());
		return false;
	}

	this->scanDirectoryRecursively(this->rootPath, this->topDirectory);

	return true;
}

unsigned int BoostFilesystemProvider::getRawResourceSize(const Descriptor& resource)
{
	bfs::path filePath = this->rootPath;
	filePath /= bfs::path(resource.getPath());
	filePath.make_preferred();

	return this->getFileSize(filePath.string());
}

unsigned int BoostFilesystemProvider::getRawResource(const Descriptor& resource, char* buffer)
{
	bfs::path filePath = this->rootPath;
	filePath /= bfs::path(resource.getPath());
	filePath.make_preferred();
	std::string filePathString = filePath.string();

	if (this->filePathToNumberMap.find(filePathString) == this->filePathToNumberMap.end())
	{
		return 0;
	}

	std::ifstream fileStream(filePathString, std::ios::binary);
	if (!fileStream)
	{
		return 0;
	}

	unsigned int fileNumber = this->filePathToNumberMap[filePathString];
	unsigned int fileSize = this->fileInfoVector[fileNumber].size;
	fileStream.read(buffer, fileSize);

	return fileSize;
}

unsigned int BoostFilesystemProvider::getNumResources() const
{
	return static_cast<unsigned int>(this->fileInfoVector.size());
}

std::string BoostFilesystemProvider::getResourcePath(unsigned int resourceNumber) const
{
	assert(resourceNumber < this->fileInfoVector.size());

	std::string filePath = this->fileInfoVector[resourceNumber].path.string();
	std::string::size_type rootStart = filePath.find(this->rootPath.string());
	if (rootStart != std::string::npos) {
		filePath.erase(rootStart, this->rootPath.string().size() + 1);
	}

	return filePath;
}

void BoostFilesystemProvider::scanDirectoryRecursively(const bfs::path& dirPath, Directory& currentDirectory)
{
	for (bfs::directory_iterator fileIt(dirPath); fileIt != bfs::directory_iterator(); fileIt++)
	{
		bfs::path filePath = fileIt->path();

		if (this->ignoreHiddenFiles == false || this->fileIsHidden(filePath) == false)
		{
			if (bfs::is_directory(filePath) == true)
			{
				Directory childDirectory(filePath.filename().string());
				this->scanDirectoryRecursively(filePath, childDirectory);

				currentDirectory.addSubdirectory(childDirectory);
			}
			else if (bfs::is_regular_file(filePath) == true)
			{
				this->filePathToNumberMap[filePath.string()] = static_cast<unsigned int>(this->fileInfoVector.size());
				const auto fileSize = bfs::file_size(filePath);
				this->fileInfoVector.push_back(FileInfo{static_cast<unsigned int>(fileSize), filePath.string()});

				const std::string relativePath = this->removeRootSegment(filePath).generic_string();
				currentDirectory.addResource(Descriptor(relativePath));
			}
		}
	}
}

unsigned int BoostFilesystemProvider::getFileSize(std::string path) const
{
	auto mapIt = this->filePathToNumberMap.find(path);
	if (mapIt == this->filePathToNumberMap.end()) {
		return 0;
	}

	unsigned int fileNumber = mapIt->second;
	assert(fileNumber < this->fileInfoVector.size());

	return this->fileInfoVector[fileNumber].size;
}

bool BoostFilesystemProvider::fileIsHidden(const bfs::path& filePath) const
{
	// TODO: Should do checking for Windows as well.
	return (filePath.filename().string().front() == '.');
}

bfs::path BoostFilesystemProvider::removeRootSegment(const bfs::path& path) const
{
	bfs::path modifiedPath;

	auto pathIt = path.begin();
	bool matchesRoot = true;

	for (const bfs::path& rootPathElement : this->rootPath)
	{
		if (rootPathElement != *pathIt)
		{
			matchesRoot = false;;
		}

		pathIt++;
	}

	if (matchesRoot == false)
	{
		return path;
	}
	else
	{
		while (pathIt != path.end())
		{
			modifiedPath /= *pathIt;

			pathIt++;
		}

		return modifiedPath;
	}
}

}
} }
