/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/Geometry.h>
#include <nox/app/graphics/GeometrySet.h>
#include <glm/gtx/rotate_vector.hpp>

namespace nox { namespace app
{
namespace graphics
{

Geometry::Geometry(GeometrySet* container):
	container(container)
{
}

Geometry::~Geometry() = default;

void Geometry::signalChange()
{
	this->container->onGeometryChanged(this);
}

Triangle::Triangle(GeometrySet* container):
	Geometry(container)
{
}

void Triangle::setColor(const glm::vec4& color)
{
	this->a.color = color;
	this->b.color = color;
	this->c.color = color;
}

std::vector<ObjectCoordinate> Triangle::generateRenderableVertices() const
{
	std::vector<ObjectCoordinate> coords(this->getNumVertices());
	coords[0] = this->a;
	coords[1] = this->b;
	coords[2] = this->c;

	return coords;
}

unsigned int Triangle::getNumVertices() const
{
	return 3;
}

unsigned int Triangle::getNumRenderObjects() const
{
    return 1;
}

GeometryType Triangle::getType() const
{
	return GeometryType::TRIANGLE;
}

GeometryPrimitive Triangle::getPrimitiveType() const
{
	return GeometryPrimitive::TRIANGLE;
}

void Triangle::setVertexA(const glm::vec2& vertex)
{
	this->a.geometryVertex = vertex;
}

void Triangle::setVertexB(const glm::vec2& vertex)
{
	this->b.geometryVertex = vertex;
}

void Triangle::setVertexC(const glm::vec2& vertex)
{
	this->c.geometryVertex = vertex;
}

void Triangle::setVertices(const glm::vec2& a, const glm::vec2& b, const glm::vec2& c)
{
	this->a.geometryVertex = a;
	this->b.geometryVertex = b;
	this->c.geometryVertex = c;
}

void Triangle::setColorA(const glm::vec4& color)
{
	this->a.color = color;
}

void Triangle::setColorB(const glm::vec4& color)
{
	this->b.color = color;
}

void Triangle::setColorC(const glm::vec4& color)
{
	this->c.color = color;
}

void Triangle::setColors(const glm::vec4& a, const glm::vec4& b, const glm::vec4& c)
{
	this->a.color = a;
	this->b.color = b;
	this->c.color = c;
}

glm::vec2 Triangle::getVertexA() const
{
	return this->a.geometryVertex;
}
glm::vec2 Triangle::getVertexB() const
{
	return this->b.geometryVertex;
}
glm::vec2 Triangle::getVertexC() const
{
	return this->c.geometryVertex;
}

Quad::Quad(GeometrySet* container):
	Geometry(container)
{
}

void Quad::setColor(const glm::vec4& color)
{
	this->bottomLeft.color = color;
	this->bottomRight.color = color;
	this->topLeft.color = color;
	this->topRight.color = color;
}

std::vector<ObjectCoordinate> Quad::generateRenderableVertices() const
{
	std::vector<ObjectCoordinate> coords(this->getNumVertices());
	coords[0] = this->bottomLeft;
	coords[1] = this->bottomRight;
	coords[2] = this->topRight;
	coords[3] = this->bottomLeft;
	coords[4] = this->topRight;
	coords[5] = this->topLeft;

	return coords;
}

unsigned int Quad::getNumVertices() const
{
	// Two triangles.
	return 6;
}

unsigned int Quad::getNumRenderObjects() const
{
    return 2;
}

GeometryType Quad::getType() const
{
	return GeometryType::QUAD;
}

GeometryPrimitive Quad::getPrimitiveType() const
{
	return GeometryPrimitive::TRIANGLE;
}

void Quad::setVertexBottomLeft(const glm::vec2& vertex)
{
	this->bottomLeft.geometryVertex = vertex;
}

void Quad::setVertexBottomRight(const glm::vec2& vertex)
{
	this->bottomRight.geometryVertex = vertex;
}

void Quad::setVertexTopRight(const glm::vec2& vertex)
{
	this->topRight.geometryVertex = vertex;
}

void Quad::setVertexTopLeft(const glm::vec2& vertex)
{
	this->topLeft.geometryVertex = vertex;
}

void Quad::setVertices(const glm::vec2& bottomLeft, const glm::vec2& bottomRight, const glm::vec2& topRight, const glm::vec2& topLeft)
{
    this->bottomLeft.geometryVertex = bottomLeft;
    this->bottomRight.geometryVertex = bottomRight;
    this->topRight.geometryVertex = topRight;
    this->topLeft.geometryVertex = topLeft;
}

void Quad::setColorBottomLeft(const glm::vec4& color)
{
	this->bottomLeft.color = color;
}

void Quad::setColorBottomRight(const glm::vec4& color)
{
	this->bottomRight.color = color;
}

void Quad::setColorTopRight(const glm::vec4& color)
{
	this->topRight.color = color;
}

void Quad::setColorTopLeft(const glm::vec4& color)
{
	this->topLeft.color = color;
}

void Quad::setColors(const glm::vec4& bottomLeft, const glm::vec4& bottomRight, const glm::vec4& topRight, const glm::vec4& topLeft)
{
    this->bottomLeft.color = bottomLeft;
    this->bottomRight.color = bottomRight;
    this->topRight.color = topRight;
    this->topLeft.color = topLeft;
}

Line::Line(GeometrySet* container):
	Geometry(container)
{
}

void Line::setColor(const glm::vec4& color)
{
	this->start.color = color;
	this->end.color = color;
}

std::vector<ObjectCoordinate> Line::generateRenderableVertices() const
{
	std::vector<ObjectCoordinate> coords(this->getNumVertices());
	coords[0] = this->start;
	coords[1] = this->end;

	return coords;
}

unsigned int Line::getNumVertices() const
{
	return 2;
}

unsigned int Line::getNumRenderObjects() const
{
	return 1;
}

GeometryType Line::getType() const
{
	return GeometryType::LINE;
}

GeometryPrimitive Line::getPrimitiveType() const
{
	return GeometryPrimitive::LINE;
}

const glm::vec2& Line::getLineBegin() const
{
	return this->start.geometryVertex;
}

const glm::vec2& Line::getLineEnd() const
{
	return this->end.geometryVertex;
}

void Line::setLineBegin(const glm::vec2& vertex)
{
	this->start.geometryVertex = vertex;
}

void Line::setLineEnd(const glm::vec2& vertex)
{
	this->end.geometryVertex = vertex;
}

void Line::setVertices(const glm::vec2& start, const glm::vec2& end)
{
	this->start.geometryVertex = start;
	this->end.geometryVertex = end;
}

void Line::setColorStart(const glm::vec4& color)
{
	this->start.color = color;
}

void Line::setColorEnd(const glm::vec4& color)
{
	this->end.color = color;
}

void Line::setColors(const glm::vec4& start, const glm::vec4& end)
{
	this->start.color = start;
	this->end.color = end;
}

Polygon::Polygon(GeometrySet* container):
	Geometry(container),
	numVertices(0),
	numTriangles(0)
{
}

void Polygon::init(unsigned int numVertices)
{
	assert(numVertices >= 3);

	this->numVertices = numVertices;
	if (this->coordinates.size() < this->numVertices)
	{
		this->coordinates.resize(this->numVertices);
	}

	if (this->numVertices == 3)
	{
		this->numTriangles = 1;
	}
	else if (this->numVertices == 4)
	{
		this->numTriangles = 2;
	}
	else
	{
		this->numTriangles = this->numVertices;
	}
}

void Polygon::setColor(const glm::vec4& color)
{
	this->color = color;
	for (unsigned int i = 0; i < this->numVertices; i++)
	{
		this->coordinates[i].color = color;
	}
}

std::vector<ObjectCoordinate> Polygon::generateRenderableVertices() const
{
	std::vector<ObjectCoordinate> vertices(this->numTriangles * 3);

	// Triangle.
	if (this->numVertices == 3)
	{
		for (unsigned int i = 0; i < this->numVertices; i++)
		{
			vertices[i].color = this->coordinates[i].color;
			vertices[i].geometryVertex = this->coordinates[i].geometryVertex;
		}
	}
	// Rectangle.
	else if (this->numVertices == 4)
	{
		std::vector<ObjectCoordinate>::size_type dataIndex = 0;

		for (unsigned int i = 0; i < 3; i++)
		{
			vertices[dataIndex].color = this->coordinates[i].color;
			vertices[dataIndex].geometryVertex = this->coordinates[i].geometryVertex;
			dataIndex++;
		}

		for (unsigned int i = 2; i < 5; i++)
		{
			vertices[dataIndex].color = this->coordinates[i % 4].color;
			vertices[dataIndex].geometryVertex = this->coordinates[i % 4].geometryVertex;
			dataIndex++;
		}
	}
	else
	{
		glm::vec2 vertexSum;

		for (unsigned int i = 0; i < this->numVertices; i++)
		{
			vertexSum.x += this->coordinates[i].geometryVertex.x;
			vertexSum.y += this->coordinates[i].geometryVertex.y;
		}

		glm::vec2 centroid;
		centroid.x = vertexSum.x / (float)this->numVertices;
		centroid.y = vertexSum.y / (float)this->numVertices;

		unsigned int numTriangles = 0;

		// Creates a fan of triangles.
		for (unsigned int i = 0; i < this->numVertices; i++)
		{
			unsigned int vertexStartPos = i * 3;
			unsigned int secondPos = i;

			unsigned int thirdPos;
			if (i < this->numVertices - 1)
			{
				thirdPos = i + 1;
			}
			else
			{
				thirdPos = 0;
			}

			vertices[vertexStartPos].color = this->coordinates[i].color;
			vertices[vertexStartPos].geometryVertex = centroid;

			vertices[vertexStartPos + 1] = this->coordinates[secondPos];
			vertices[vertexStartPos + 2] = this->coordinates[thirdPos];

			numTriangles++;
		}
	}

	return vertices;
}

unsigned int Polygon::getNumVertices() const
{
	// 3 vertices per triangle.
	return this->numTriangles * 3;
}

GeometryType Polygon::getType() const
{
	return GeometryType::POLYGON;
}

GeometryPrimitive Polygon::getPrimitiveType() const
{
	return GeometryPrimitive::TRIANGLE;
}

void Polygon::setVertex(unsigned int vertexNum, const glm::vec2& vertex)
{
	assert(vertexNum < this->numVertices);
	this->coordinates[vertexNum].geometryVertex = vertex;
}

void Polygon::setColor(unsigned int vertexNum, const glm::vec4& color)
{
	assert(vertexNum < this->numVertices);
	this->coordinates[vertexNum].color = color;
}

unsigned int Polygon::getNumRenderObjects() const
{
	return this->numTriangles;
}

LineLoop::LineLoop(GeometrySet* container):
	Geometry(container),
	numVertices(0),
	numLines(0),
	closed(true)
{
}

void LineLoop::setColor(const glm::vec4& color)
{
	this->color = color;
	for (unsigned int i = 0; i < this->numVertices; i++)
	{
		this->coordinates[i].color = color;
	}
}

std::vector<ObjectCoordinate> LineLoop::generateRenderableVertices() const
{
	std::vector<ObjectCoordinate> coords(this->numLines * 2);

	for (unsigned int i = 0; i < this->numLines; i++)
	{
		unsigned int current = i * 2;
		unsigned int next = current + 1;

		coords[current] = this->coordinates[i];
		coords[next] = this->coordinates[(i + 1) % this->numVertices];
	}

	return coords;
}

unsigned int LineLoop::getNumVertices() const
{
	return this->numLines * 2;
}

unsigned int LineLoop::getNumRenderObjects() const
{
	return this->numLines;
}

GeometryType LineLoop::getType() const
{
	return GeometryType::LINELOOP;
}

GeometryPrimitive LineLoop::getPrimitiveType() const
{
	return GeometryPrimitive::LINE;
}

void LineLoop::init(unsigned int numVertices, bool closed)
{
	assert(numVertices > 1);
	this->numVertices = numVertices;
	this->closed = closed;

	if (this->closed == true)
	{
		this->numLines = this->numVertices;
	}
	else
	{
		this->numLines = this->numVertices - 1;
	}

	if (this->coordinates.size() < this->numVertices)
	{
		this->coordinates.resize(this->numVertices);
	}
}

void LineLoop::setVertex(unsigned int vertexNum, const glm::vec2& vertex)
{
	assert(vertexNum < this->numVertices);
	this->coordinates[vertexNum].geometryVertex = vertex;
}

void LineLoop::setColor(unsigned int vertexNum, const glm::vec4& color)
{
	assert(vertexNum < this->numVertices);
	this->coordinates[vertexNum].color = color;
}

void Triangle::setAsIsosceles(const glm::vec2& position, const float angle, const float width, const float length)
{
	glm::vec2 left = glm::vec2(0.0f, width / 2.0f);
	glm::vec2 right = glm::vec2(0.0f, -width / 2.0f);
	glm::vec2 front = glm::vec2(length, 0.0f);

	left = glm::rotate(left, angle);
	right = glm::rotate(right, angle);
	front = glm::rotate(front, angle);

	left += position;
	right += position;
	front += position;

	this->setVertices(left, right, front);
}

}
} }
