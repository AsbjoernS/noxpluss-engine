/*
* NOX Engine
*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include <glm/gtc/matrix_transform.hpp>
#include <nox/app/graphics/TrdCamera.h>
#include <glew/glew.h>
#include <iostream>

namespace nox {
	namespace app
	{
		namespace graphics
		{
			TrdCamera::TrdCamera(const glm::uvec2& size) :
				cameraSize(size),
				zfar(1000.0f),
				znear(0.1f),
				fov(45.0f),
				upDirection(glm::vec3(0.0f, 1.0f, 0.0f)),
				viewDirection(glm::vec3(0.0f, 0.0f, -1.0f)),
				cameraPosition(glm::vec3(0.0f, 10.0f, 0.0f)),
				moveSpeed(0.1f),
				oldMousePosition(glm::vec2(0, 0))
			{
				this->projectionMatrix = glm::perspective(fov, 800.0f / 600.0f, znear, zfar);
			}

			 glm::mat4x4& TrdCamera::getViewProjectionMatrix() 
			{
				updateViewMatrix();
				return this->viewProjectionMatrix;
			}

			const glm::mat4x4& TrdCamera::getViewMatrix() const
			{
				return this->viewMatrix;
			}

			void TrdCamera::updateViewMatrix()
			{
				this->viewMatrix = glm::lookAt(
					cameraPosition,						// Camera position in World Space
					cameraPosition + viewDirection,		// and looks at the origin
					upDirection							// Head is up (set to 0,-1,0 to look upside-down)
					);
				this->viewProjectionMatrix = this->projectionMatrix * this->viewMatrix;
			}

			void TrdCamera::setPosition(const glm::vec3 &position)
			{
				this->cameraPosition = position;
				
			}

			glm::vec3 TrdCamera::getPosition() const
			{
				return this->cameraPosition;
			}

			void TrdCamera::setScale(const glm::vec2& scale)
			{
				this->cameraScale = scale;
				
			}

			glm::vec2 TrdCamera::getScale() const
			{
				return this->cameraScale;
			}

		

			void TrdCamera::setSize(const glm::uvec2& size)
			{
				this->cameraSize = size;
			
			}

			glm::vec2 TrdCamera::getSize() const
			{
				
				return cameraSize;
			}

			void TrdCamera::moveForward()
			{
				cameraPosition += moveSpeed * viewDirection;
			
			}
			void TrdCamera::moveBackward()
			{
				cameraPosition += -moveSpeed * viewDirection;
			
			}
			void TrdCamera::moveLeft()
			{
				glm::vec3 left = glm::cross(viewDirection, upDirection );
				cameraPosition += -moveSpeed * left;
			}
			void TrdCamera::moveRight()
			{
				glm::vec3 right = glm::cross(viewDirection, upDirection);
				cameraPosition += moveSpeed * right;
			}

			void TrdCamera::mouseUpdate(int x, int y)
			{
				glm::vec2 newMousePosition;
				newMousePosition.x = x;
				newMousePosition.y = y;
				//How much the mouse has moved
				glm::vec2 mouseDelta = newMousePosition - oldMousePosition;
				//rotate mouseDelta.x (90 pixels = 90 degrees) around the up vector, applying the rotation to the viewDirection vector:
				//rotate returns a matrix 4x4 and viewDirection is a vec3:
				if (glm::length(mouseDelta) < MOUSE_MOVE_MAX) {
					viewDirection = glm::mat3(glm::rotate(glm::mat4(), -mouseDelta.x * ROTATIONAL_SPEED, upDirection)) * viewDirection;

					glm::vec3 toRotateAround = glm::cross(viewDirection, upDirection);
					viewDirection = glm::mat3(glm::rotate(glm::mat4(), -mouseDelta.y * ROTATIONAL_SPEED, toRotateAround)) * viewDirection;					
				}
				
				oldMousePosition = newMousePosition;
			}
			void TrdCamera::reset()
			{
				this->cameraPosition = glm::vec3(0.0f, 0.0f, 15.0f);
				this->viewDirection = glm::vec3(0.0f, 0.0f, -1.0f);
				updateViewMatrix();
			}
		}
	}
}
