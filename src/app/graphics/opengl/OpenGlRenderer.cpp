/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/IContext.h>
#include <nox/app/graphics/opengl/DebugRenderer.h>
#include <nox/app/graphics/opengl/TiledTextureRenderer.h>
#include <nox/app/graphics/opengl/StenciledTiledTextureRenderer.h>
#include <nox/app/graphics/opengl/opengl_utils.h>
#include <nox/app/graphics/opengl/BackgroundGradient.h>
#include <nox/app/graphics/SubRenderer.h>
#include <nox/app/graphics/SceneGraphNode.h>
#include <nox/app/graphics/TransformationNode.h>
#include <nox/app/graphics/StenciledTiledTextureGenerator.h>
#include <nox/app/graphics/Geometry.h>
#include <nox/app/graphics/Light.h>
#include <nox/app/graphics/opengl/OpenGlRenderer.h>
#include <nox/app/graphics/Camera.h>
#include <nox/app/resource/Handle.h>
#include <json/reader.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <nox/app/resource/IResourceAccess.h>
#include <nox/util/algorithm.h>
#include <SDL2/SDL.h>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cstring>

namespace nox { namespace app { namespace graphics
{

namespace
{

std::vector<TextureQuad::VertexAttribute> makeTriangleStripBox(const math::Box<glm::vec2>& box);

}

OpenGlRenderer::OpenGlRenderer():
	NUM_RENDER_LEVELS(10000),

	lightStartRenderLevel(0),
	renderCamera(std::make_shared<Camera>(glm::uvec2(1, 1))),
	debugRenderingEnabled(false),
	windowSizeChanged(false),

	lightLevel(1.0f),

	lightClearColor(glm::vec4(0)),
	renderClearColor(glm::vec4(0)),

	renderTexture(0),
	lightLuminanceTexture(0),

	superSampleMultiplier(1.0f),
	lightMapMultisamplingSamples(0),
	lightTextureSizeMultiplier(0.3f),
	effectTextureSizeMultiplier(0.3f),

	litGameplayPresentVAO(0),
	litGameplayPresentVBO(0),
	litGameplayPresentProgram(0),
	litGameplayPresentTexture(0),

	litGameplayPresentMatrixUniform(-1),
	litGameplayPresentRenderUniform(-1),
	litGameplayPresentLightUniform(-1),
	litGameplayPresentLightLuminanceUniform(-1),
	litGameplayPresentDayLightCoefficiencyUniform(-1),

	effectGameplayPresentVAO(0),
	effectGameplayPresentVBO(0),
	effectGameplayPresentProgram(0),

	effectGameplayPresentMatrixUniform(-1),
	effectGameplayPresentRenderUniform(-1),
	effectGameplayPresentLightUniform(-1),

	effectMapVAO(0),
	effectMapVBO(0),
	effectMapTexture(0),

	lightMapVAO(0),
	lightMapVBO(0),
	lightMapTexture(0),

	lightHorizontalBlurVAO(0),
	lightHorizontalBlurVBO(0),
	lightHorizontalBlurProgram(0),
	lightHorizontalBlurTexture(0),

	lightHorizontalBlurMatrixUniform(-1),
	lightHorizontalBlurTextureUniform(-1),
	lightHorizontalBlurSizeUniform(-1),

	lightVerticalBlurVAO(0),
	lightVerticalBlurVBO(0),
	lightVerticalBlurProgram(0),
	lightVerticalBlurTexture(0),

	lightVerticalBlurMatrixUniform(0),
	lightVerticalBlurTextureUniform(0),
	lightVerticalBlurSizeUniform(0),

	lightTexture(0),

	blurEnabled(true),
	pixelsPerBlurStep(1),
	numberOfStaticLightCoords(0),
	staticLightsUpdated(false),
	numberOfDynamicLightCoords(0),
	dynamicLightsUpdated(false),
	lightDataSize(0),
	totalLightCoords(0),
	numberOfEffectCoords(0),
	effectsUpdated(false),
	effectDataSize(0),
	totalEffectCoords(0),
	textureRenderer(NUM_RENDER_LEVELS),
	worldAtlasTextureBuffer(0),

	debugDataPreparationStartBarrier(2),
	debugDataPreparationFinishedBarrier(2),
	quitDebugDataPreparation(false),

	textureDataPreparationStartBarrier(2),
	textureDataPreparationFinishedBarrier(2),
	quitTextureDataPreparation(false),

	lightingDataPreparationStartBarrier(2),
	lightingDataPreparationFinishedBarrier(2),
	quitLightingDataPreparation(false),

	effectDataPreparationStartBarrier(2),
	effectDataPreparationFinishedBarrier(2),
	quitEffectDataPreparation(false),

	subRenderersDataPreparationStartBarrier(2),
	subRenderersDataPreparationFinishedBarrier(2),
	quitSubRenderersDataPreparation(false)
{
	this->debugRenderer = std::unique_ptr<DebugRenderer>(new DebugRenderer());
}

OpenGlRenderer::~OpenGlRenderer()
{
	this->quitDebugDataPreparation = true;
	this->debugDataPreparationStartBarrier.wait();
	this->debugDataPreparationFinishedBarrier.wait();

	if (debugDataPreparerThread.joinable())
	{
		debugDataPreparerThread.join();
	}

	this->quitTextureDataPreparation = true;
	this->textureDataPreparationStartBarrier.wait();
	this->textureDataPreparationFinishedBarrier.wait();

	if (textureDataPreparerThread.joinable())
	{
		textureDataPreparerThread.join();
	}

	this->quitLightingDataPreparation = true;
	this->lightingDataPreparationStartBarrier.wait();
	this->lightingDataPreparationFinishedBarrier.wait();

	if (lightingDataPreparerThread.joinable())
	{
		lightingDataPreparerThread.join();
	}

	this->quitEffectDataPreparation = true;
	this->effectDataPreparationStartBarrier.wait();
	this->effectDataPreparationFinishedBarrier.wait();

	if (effectDataPreparerThread.joinable())
	{
		effectDataPreparerThread.join();
	}

	this->quitSubRenderersDataPreparation = true;
	this->subRenderersDataPreparationStartBarrier.wait();
	this->subRenderersDataPreparationFinishedBarrier.wait();

	if (subRenderersDataPreparerThread.joinable())
	{
		subRenderersDataPreparerThread.join();
	}
}

bool OpenGlRenderer::init(IContext* context, const std::string& shaderDirectory, const glm::uvec2& windowSize)
{
	assert(context != nullptr);

	this->log = context->createLogger();
	this->log.setName("OpenGLRenderer");

	this->textureManager.setLogger(context->createLogger());

	resource::IResourceAccess* resourceCache = context->getResourceAccess();
	assert(resourceCache != nullptr);

	if (this->initOpenGL(windowSize) == false)
	{
		return false;
	}

	if (this->setupSpriteRendering(resourceCache, shaderDirectory) == false)
	{
		return false;
	}

	if (this->setupDebugRendering(resourceCache, shaderDirectory) == false)
	{
		return false;
	}

	if (this->setupLightMapRendering(resourceCache, shaderDirectory) == false)
	{
		return false;
	}

	if (this->setupEffectMapRendering(resourceCache, shaderDirectory) == false)
	{
		return false;
	}

	if (this->setupLitWorldRendering(resourceCache, shaderDirectory) == false)
	{
		return false;
	}

	if (this->setupEffectWorldRendering(resourceCache, shaderDirectory) == false)
	{
		return false;
	}

	if (this->setupBackgroundGradientRendering(resourceCache, shaderDirectory) == false)
	{
		return false;
	}

	if (this->blurEnabled == true)
	{
		if (this->setupLightHorizontalBlur(resourceCache, shaderDirectory) == false)
		{
			return false;
		}

		if (this->setupLightVerticalBlur(resourceCache, shaderDirectory) == false)
		{
			return false;
		}
	}

	for (auto& subRenderer : this->subRenderers)
	{
		subRenderer->init(this->renderData, windowSize.x, windowSize.y);
	}

	if (this->blurEnabled == true)
	{
		this->lightTexture = this->lightVerticalBlurTexture;
	}
	else
	{
		this->lightTexture = this->lightMapTexture;
	}

	this->debugDataPreparerThread = std::thread(&OpenGlRenderer::debugDataPreparation, this);
	this->textureDataPreparerThread = std::thread(&OpenGlRenderer::textureDataPreparation, this);
	this->lightingDataPreparerThread = std::thread(&OpenGlRenderer::lightingDataPreparation, this);
	this->effectDataPreparerThread = std::thread(&OpenGlRenderer::effectDataPreparation, this);
	this->subRenderersDataPreparerThread = std::thread(&OpenGlRenderer::subRenderersDataPreparation, this);

	return true;
}

void OpenGlRenderer::loadTextureAtlases(const resource::Descriptor& graphicsResourceDescriptor, resource::IResourceAccess* resourceAccess)
{
	this->textureManager.readGraphicsFile(graphicsResourceDescriptor, resourceAccess);
	this->textureManager.writeToTextureBuffers(resourceAccess);

}

void OpenGlRenderer::setWorldTextureAtlas(const std::string& atlasName)
{
	this->worldAtlasTextureBuffer = this->textureManager.getTextureBuffer(atlasName);
}

void OpenGlRenderer::onRender()
{
	if (this->debugRenderingEnabled == true)
	{
		this->debugDataPreparationStartBarrier.wait();
	}

	// Start asynchronus data preparation.
	this->textureDataPreparationStartBarrier.wait();
	this->lightingDataPreparationStartBarrier.wait();
	this->effectDataPreparationStartBarrier.wait();
	this->subRenderersDataPreparationStartBarrier.wait();

	glFinish();

	this->effectDataPreparationFinishedBarrier.wait();
	this->effectRenderingIo();

	//Start asynchronus I/O.
	this->lightingDataPreparationFinishedBarrier.wait();
	this->lightRenderingIo();

	const glm::vec2 cameraPosition = this->renderCamera->getPosition();
	const glm::vec2 cameraHalfSize = this->renderCamera->getSize() / 2.0f;
	this->textureDataPreparationFinishedBarrier.wait();
	this->textureRenderer.setCullArea(math::Box<glm::vec2>(cameraPosition - cameraHalfSize, cameraPosition + cameraHalfSize), this->renderCamera->getRotation());
	this->textureRenderer.handleIo(this->renderData);

	if (this->backgroundGradient != nullptr && this->backgroundGradient->needsBufferUpdate() == true)
	{
		this->backgroundGradient->handleIo(this->renderData);
	}

	if (this->debugRenderingEnabled == true)
	{
		this->debugDataPreparationFinishedBarrier.wait();
		this->debugRenderer->onDebugIo(this->renderData, this->renderCamera->getViewProjectionMatrix());
	}

	//  Render the world to a texture.
	this->renderWorld();

	glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	//  Render the light map to a texture;
	this->onLightMapRender();

	//  Render the effect map to a texture;
	this->onEffectMapRender();

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//  Render the world lit by the light map
	this->renderLitWorld();

	//  Render the lit world with effects.
	this->renderEffectWorldToScreen();

	//  Render the debug objects.
	if (this->debugRenderingEnabled == true)
	{
		std::lock_guard<std::mutex> debugLock(this->debugRenderMutex);

		this->debugRenderer->onDebugRender(this->renderData);
	}

	this->subRenderersDataPreparationFinishedBarrier.wait();
	for (auto& subRenderer : this->subRenderers)
	{
		subRenderer->render(this->renderData);
	}

	if (this->windowSizeChanged == true)
	{
	        this->windowSizeChanged = false;
	}

#ifndef NDEBUG
	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("OpenGL: %i", error);
	}
#endif
}



void OpenGlRenderer::setRootSceneNode(const std::shared_ptr<SceneGraphNode>& rootNode)
{
    this->sceneRoot = rootNode;
    this->sceneRoot->setCurrentRenderer(this);
}

void OpenGlRenderer::setCamera(const std::shared_ptr<Camera>& camera)
{
    this->renderCamera = camera;
}

bool OpenGlRenderer::toggleDebugRendering()
{
    this->debugRenderingEnabled = !this->debugRenderingEnabled;

    return this->debugRenderingEnabled;
}

void OpenGlRenderer::parseSceneGraph()
{
	if (this->sceneRoot != nullptr)
	{
		glm::mat4 transformationMatrix;
		this->sceneRoot->onTraverse(this->textureRenderer, transformationMatrix);
	}
}

void OpenGlRenderer::renderObjects()
{
	this->textureRenderer.drawData(this->renderData);
}

void OpenGlRenderer::renderWorld()
{
	this->gameplayFBO.bind(GL_FRAMEBUFFER);
	if (this->renderClearColor != this->currentClearColor)
	{
		glClearColor(renderClearColor.r, renderClearColor.g, renderClearColor.b, renderClearColor.a);
		this->currentClearColor = this->renderClearColor;
	}

	this->renderData.setStencilMask(0xFF);
	glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// We need to set the viewport to fit the new resolution (if we use supersampling).
	if (this->superSampleMultiplier != 1.0f)
	{
		glm::vec2 supersampledSize;
		supersampledSize.x = (float)this->windowSize.x * this->superSampleMultiplier;
		supersampledSize.y = (float)this->windowSize.y * this->superSampleMultiplier;

		glViewport(0, 0, (GLsizei)supersampledSize.x, (GLsizei)supersampledSize.y);
	}

	const glm::mat4& viewProjectionMatrix = this->renderCamera->getViewProjectionMatrix();

	assert(this->worldAtlasTextureBuffer > 0);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->worldAtlasTextureBuffer);

	this->renderData.bindShaderProgram(this->shaders["spriteLightLuminance"].shaderProgram);
	glUniformMatrix4fv(this->shaders["spriteLightLuminance"].ViewProjectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(viewProjectionMatrix));

	this->renderData.bindShaderProgram(this->shaders["sprite"].shaderProgram);
	glUniformMatrix4fv(this->shaders["sprite"].ViewProjectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(viewProjectionMatrix));

	for (std::unique_ptr<RenderStep>& renderStep : this->renderSteps)
	{
		renderStep->render(this->renderData, viewProjectionMatrix);
	}

	if (this->superSampleMultiplier != 1.0f)
	{
		glViewport(0, 0, static_cast<GLsizei>(this->windowSize.x), static_cast<GLsizei>(this->windowSize.y));
	}
}

void OpenGlRenderer::renderLitWorld()
{
	this->litGameplayPresentFBO.bind(GL_FRAMEBUFFER);

	if (this->lightClearColor != this->currentClearColor)
	{
		glClearColor(this->lightClearColor.r, this->lightClearColor.g, this->lightClearColor.b, this->lightClearColor.a);
		this->currentClearColor = this->lightClearColor;
	}
	glClear(GL_COLOR_BUFFER_BIT);

	this->renderData.bindShaderProgram(this->litGameplayPresentProgram);
	this->renderData.bindVertexArray(this->litGameplayPresentVAO);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->renderTexture);

	glActiveTexture(GL_TEXTURE1);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->lightTexture);
	glUniform1i(this->litGameplayPresentLightUniform, 1);

	glActiveTexture(GL_TEXTURE2);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->lightLuminanceTexture);
	glUniform1i(this->litGameplayPresentLightLuminanceUniform, 2);

	glActiveTexture(GL_TEXTURE0);

	glUniform1f(this->litGameplayPresentDayLightCoefficiencyUniform, this->lightLevel);

	if (this->windowSizeChanged)
	{
		glm::mat4x4 modelViewProjectionMatrix = glm::ortho(0.0f, (float)this->windowSize.x, 0.0f, (float)this->windowSize.y, -1.0f, 1.0f);
		glUniformMatrix4fv(this->litGameplayPresentMatrixUniform, 1, GL_FALSE,glm::value_ptr(modelViewProjectionMatrix));

		glActiveTexture(GL_TEXTURE0);
		this->renderData.bindTexture(GL_TEXTURE_2D, this->renderTexture);
		glUniform1i(this->litGameplayPresentRenderUniform, 0);

		glActiveTexture(GL_TEXTURE1);
		this->renderData.bindTexture(GL_TEXTURE_2D, this->lightTexture);
		glUniform1i(this->litGameplayPresentLightUniform, 1);

		this->renderData.bindBuffer(GL_ARRAY_BUFFER, this->litGameplayPresentVBO);
	
		std::vector<TextureQuad::VertexAttribute> coords = makeTriangleStripBox(math::Box<glm::vec2>({0.0f, 0.0f}, {this->windowSize.x, this->windowSize.y}));
		glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(coords.size() * sizeof(TextureQuad::VertexAttribute)), coords.data(), GL_STATIC_DRAW);
	}

	renderData.disable(GL_STENCIL_TEST);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void OpenGlRenderer::renderEffectWorldToScreen()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	if (this->lightClearColor != this->currentClearColor)
	{
		glClearColor(lightClearColor.r, lightClearColor.g, lightClearColor.b, lightClearColor.a);
		this->currentClearColor = this->lightClearColor;
	}
	glClear(GL_COLOR_BUFFER_BIT);

	this->renderData.bindShaderProgram(effectGameplayPresentProgram);
	this->renderData.bindVertexArray(effectGameplayPresentVAO);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->litGameplayPresentTexture);

	glActiveTexture(GL_TEXTURE1);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->effectMapTexture);
	glUniform1i(this->effectGameplayPresentLightUniform, 1);
	glActiveTexture(GL_TEXTURE0);


	if (this->windowSizeChanged)
	{
		glm::mat4x4 modelViewProjectionMatrix = glm::ortho(0.0f, (float)this->windowSize.x, 0.0f, (float)this->windowSize.y, -1.0f, 1.0f);
		glUniformMatrix4fv(this->effectGameplayPresentMatrixUniform, 1, GL_FALSE,glm::value_ptr(modelViewProjectionMatrix));

		glActiveTexture(GL_TEXTURE0);
		this->renderData.bindTexture(GL_TEXTURE_2D, this->litGameplayPresentTexture);
		glUniform1i(this->effectGameplayPresentRenderUniform, 0);

		glActiveTexture(GL_TEXTURE1);
		this->renderData.bindTexture(GL_TEXTURE_2D, this->effectMapTexture);
		glUniform1i(this->effectGameplayPresentLightUniform, 1);

		this->renderData.bindBuffer(GL_ARRAY_BUFFER, this->effectGameplayPresentVBO);

		std::vector<TextureQuad::VertexAttribute> coords = makeTriangleStripBox(math::Box<glm::vec2>({0.0f, 0.0f}, {this->windowSize.x, this->windowSize.y}));
		glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(coords.size() * sizeof(TextureQuad::VertexAttribute)), coords.data(), GL_STATIC_DRAW);

		glActiveTexture(GL_TEXTURE0);
	}

	renderData.disable(GL_STENCIL_TEST);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void OpenGlRenderer::renderLightHorizontalBlur()
{
	this->lightHorizontalBlurFBO.bind(GL_FRAMEBUFFER);
	glClear(GL_COLOR_BUFFER_BIT);

	this->renderData.bindShaderProgram(this->lightHorizontalBlurProgram);
	
	this->renderData.bindVertexArray(this->lightHorizontalBlurVAO);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->lightMapTexture);
	
	const float scaledWindowWidth = static_cast<float>(this->windowSize.x) * this->lightTextureSizeMultiplier;
	const float scaledWindowHeight = static_cast<float>(this->windowSize.y) * this->lightTextureSizeMultiplier;

	if (this->windowSizeChanged)
	{
		glm::mat4x4 modelViewProjectionMatrix = glm::ortho(0.0f, (float)this->windowSize.x, 0.0f, (float)this->windowSize.y, -1.0f, 1.0f);
		glUniformMatrix4fv(this->lightHorizontalBlurMatrixUniform, 1, GL_FALSE,glm::value_ptr(modelViewProjectionMatrix));

		const float blurSize = this->pixelsPerBlurStep / scaledWindowWidth;
		glUniform1f(this->lightHorizontalBlurSizeUniform, blurSize);

		glActiveTexture(GL_TEXTURE0);
		this->renderData.bindTexture(GL_TEXTURE_2D, this->renderTexture);
		glUniform1i(this->lightHorizontalBlurTextureUniform, 0);
		
		this->renderData.bindBuffer(GL_ARRAY_BUFFER, this->lightHorizontalBlurVBO);
		
		std::vector<TextureQuad::VertexAttribute> coords = makeTriangleStripBox(math::Box<glm::vec2>({0.0f, 0.0f}, {this->windowSize.x, this->windowSize.y}));
		glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(coords.size() * sizeof(TextureQuad::VertexAttribute)), coords.data(), GL_STATIC_DRAW);
	}

	if (this->lightTextureSizeMultiplier != 1.0f)
	{
		// We need to set the viewport to fit the new resolution.
		glViewport(0, 0, static_cast<GLint>(scaledWindowWidth), static_cast<GLint>(scaledWindowHeight));
	}

	renderData.disable(GL_STENCIL_TEST);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glViewport(0, 0, static_cast<GLsizei>(this->windowSize.x), static_cast<GLsizei>(this->windowSize.y));

}


void OpenGlRenderer::renderLightVerticalBlur()
{
	this->lightVerticalBlurFBO.bind(GL_FRAMEBUFFER);
	glClear(GL_COLOR_BUFFER_BIT);

	this->renderData.bindShaderProgram(lightVerticalBlurProgram);
	this->renderData.bindVertexArray(lightVerticalBlurVAO);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->lightHorizontalBlurTexture);

	const float scaledWindowWidth = static_cast<float>(this->windowSize.x) * this->lightTextureSizeMultiplier;
	const float scaledWindowHeight = static_cast<float>(this->windowSize.y) * this->lightTextureSizeMultiplier;

	if (this->windowSizeChanged)
	{
		glm::mat4x4 modelViewProjectionMatrix = glm::ortho(0.0f, (float)this->windowSize.x, 0.0f, (float)this->windowSize.y, -1.0f, 1.0f);
		glUniformMatrix4fv(this->lightVerticalBlurMatrixUniform, 1, GL_FALSE,glm::value_ptr(modelViewProjectionMatrix));

		float blurSize = this->pixelsPerBlurStep / scaledWindowHeight;
		glUniform1f(this->lightVerticalBlurSizeUniform, blurSize);

		glActiveTexture(GL_TEXTURE0);
		this->renderData.bindTexture(GL_TEXTURE_2D, this->lightHorizontalBlurTexture);
		glUniform1i(this->lightVerticalBlurTextureUniform, 0);
		
		this->renderData.bindBuffer(GL_ARRAY_BUFFER, this->lightVerticalBlurVBO);
		
		std::vector<TextureQuad::VertexAttribute> coords = makeTriangleStripBox(math::Box<glm::vec2>({0.0f, 0.0f}, {this->windowSize.x, this->windowSize.y}));
		glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(coords.size() * sizeof(TextureQuad::VertexAttribute)), coords.data(), GL_STATIC_DRAW);
	}

	if (this->lightTextureSizeMultiplier != 1.0f)
	{
		// We need to set the viewport to fit the new resolution.
		glViewport(0, 0, static_cast<GLint>(scaledWindowWidth), static_cast<GLint>(scaledWindowHeight));
	}

	renderData.disable(GL_STENCIL_TEST);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glViewport(0, 0, static_cast<GLsizei>(this->windowSize.x), static_cast<GLsizei>(this->windowSize.y));

}

bool OpenGlRenderer::initOpenGL(const glm::uvec2& windowSize)
{
	this->windowSize = windowSize;

	this->currentClearColor = glm::vec4(1);
	this->lightClearColor = glm::vec4(0.0f, 0.0f, 0.0f, 1.0);
	this->renderClearColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

	glewExperimental = GL_TRUE;
	GLenum glewError = glewInit();

	// GLEW might trigger an error flag that we can safely ignore, so we flush it.
	glGetError();

	if (glewError != GLEW_OK)
	{
		glewGetErrorString(glewError);
		this->log.error().format("Error initializing GLEW: %s", (const char *) glewGetErrorString(glewError));
		return false;
	}

	this->renderData.disable(GL_DEPTH_TEST);
	this->renderData.disable(GL_STENCIL_TEST);
	this->renderData.enable(GL_BLEND);
	glBlendEquationSeparate(GL_FUNC_ADD, GL_MAX);

	glClearStencil(0x00);

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Error initializing OpenGL: %d", error);
		return false;
	}


	if (this->lightMapMultisamplingSamples == 0)
	{
		this->renderData.disable(GL_MULTISAMPLE);
	}
	else
	{

		GLint maxMultisampleSamples = 0;
		glGetIntegerv(GL_MAX_SAMPLES, &maxMultisampleSamples);

		if ((int)this->lightMapMultisamplingSamples > maxMultisampleSamples)
		{
			this->lightMapMultisamplingSamples = static_cast<unsigned int>(maxMultisampleSamples);
		}

		if (this->lightMapMultisamplingSamples > 0)
		{
			this->renderData.enable(GL_MULTISAMPLE);
		}
		else
		{
			this->renderData.disable(GL_MULTISAMPLE);
		}
	}


	this->log.verbose().format("Enabling multisampling with %i samples.", this->lightMapMultisamplingSamples);
	this->log.verbose().format("Enabling %.1f times supersampling.", this->superSampleMultiplier);


	const char* glVersionString = (const char *) glGetString(GL_VERSION);
	const char* glVendorString = (const char *) glGetString(GL_VENDOR);
	const char* glslVersionString = (const char *) glGetString(GL_SHADING_LANGUAGE_VERSION);

	GLint glMajor = 0;
	GLint glMinor = 0;
	glGetIntegerv(GL_MAJOR_VERSION, &glMajor);
	glGetIntegerv(GL_MINOR_VERSION, &glMinor);

	this->renderData = RenderData(static_cast<GLuint>(glMajor), static_cast<GLuint>(glMinor), &this->textureManager);

	if (this->renderData.getGlMajorVersion() < 3)
	{
		this->log.error().format("Unsupported hardware detected: OpenGL %i.%i, with GLSL %s, using \"%s\" by \"%s\". OpenGL 3.0 or better is required.",
			this->renderData.getGlMajorVersion(),
			this->renderData.getGlMinorVersion(),
			glslVersionString,
			glVersionString,
			glVendorString);

		return false;
	}

	this->log.verbose().format(
			"Initialized OpenGL %i.%i, with GLSL %s, using \"%s\" by \"%s\".",
			this->renderData.getGlMajorVersion(),
			this->renderData.getGlMinorVersion(),
			glslVersionString,
			glVersionString,
			glVendorString);

	this->resizeWindow(this->windowSize);

	return true;
}

const TextureManager& OpenGlRenderer::getTextureManager() const
{
	return this->textureManager;
}

bool OpenGlRenderer::setupLitWorldRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	glGenVertexArrays(1, &litGameplayPresentVAO);
	this->renderData.bindVertexArray(litGameplayPresentVAO);

	const std::string SHADER_NAME = "litGameplayPresent";

	// Create shaders.
	litGameplayPresentProgram = glCreateProgram();

	const VertexAndFragmentShader shader = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
	assert(shader.isValid());

	assert(litGameplayPresentProgram != 0);

	// Bind shader attributes.
	const GLuint vertexCoordLocation = 0;
	const GLuint textureCoordLocation = 1;
	const GLuint colorLocation = 3;

	glBindAttribLocation(litGameplayPresentProgram, vertexCoordLocation, "vertex");
	glBindAttribLocation(litGameplayPresentProgram, textureCoordLocation, "textureCoord");
	glBindAttribLocation(litGameplayPresentProgram, colorLocation, "color");

	// Link shader program.
	if (linkShaderProgram(litGameplayPresentProgram, shader.vertex, shader.fragment) == false)
	{
		this->log.error().format("Could not link %s shader.", SHADER_NAME.c_str());
		return false;
	}

	this->litGameplayPresentMatrixUniform = glGetUniformLocation(litGameplayPresentProgram, "viewProjectionMatrix");
	assert(this->litGameplayPresentMatrixUniform >= 0);

	this->litGameplayPresentRenderUniform = glGetUniformLocation(litGameplayPresentProgram, "renderTexture");
	assert(this->litGameplayPresentRenderUniform >= 0);

	this->litGameplayPresentLightUniform = glGetUniformLocation(litGameplayPresentProgram, "lightMapTexture");
	assert(this->litGameplayPresentLightUniform >= 0);

	this->litGameplayPresentLightLuminanceUniform = glGetUniformLocation(litGameplayPresentProgram, "lightLuminanceTexture");
	assert(this->litGameplayPresentLightLuminanceUniform >= 0);

	this->litGameplayPresentDayLightCoefficiencyUniform = glGetUniformLocation(litGameplayPresentProgram, "dayLightCoefficiency");
	assert(this->litGameplayPresentLightUniform >= 0);

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Error = %d", error);
	}

	this->renderData.bindShaderProgram(litGameplayPresentProgram);

	glm::mat4x4 modelViewProjectionMatrix = glm::ortho(0.0f, (float) windowSize.x, 0.0f, (float) windowSize.y, -1.0f, 1.0f);

	glUniformMatrix4fv(this->litGameplayPresentMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));

	glActiveTexture(GL_TEXTURE0);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->renderTexture);
	glUniform1i(this->litGameplayPresentRenderUniform, 0);

	glActiveTexture(GL_TEXTURE1);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->lightTexture);
	glUniform1i(this->litGameplayPresentLightUniform, 1);

	glActiveTexture(GL_TEXTURE2);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->lightLuminanceTexture);
	glUniform1i(this->litGameplayPresentLightLuminanceUniform, 2);

	glActiveTexture(GL_TEXTURE0);

	glGenBuffers(1, &this->litGameplayPresentVBO);
	this->renderData.bindBuffer(GL_ARRAY_BUFFER, this->litGameplayPresentVBO);

	TextureQuad::VertexAttribute::positionAttribPointer(vertexCoordLocation);
	TextureQuad::VertexAttribute::textureCoordinateAttribPointer(textureCoordLocation);
	TextureQuad::VertexAttribute::colorAttribPointer(colorLocation);

	glEnableVertexAttribArray(vertexCoordLocation);
	glEnableVertexAttribArray(textureCoordLocation);
	glEnableVertexAttribArray(colorLocation);

	return true;
}

bool OpenGlRenderer::setupEffectWorldRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	assert(this->renderData.getGlMajorVersion() != 0);

	glGenVertexArrays(1, &effectGameplayPresentVAO);
	this->renderData.bindVertexArray(effectGameplayPresentVAO);

	this->effectGameplayPresentProgram = glCreateProgram();
	assert(this->effectGameplayPresentProgram != 0);

	const std::string SHADER_NAME = "effectGameplayPresent";

	const VertexAndFragmentShader shader = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
	assert(shader.isValid());


	// Bind shader attributes.
	const GLuint vertexCoordLocation = 0;
	const GLuint textureCoordLocation = 1;
	const GLuint colorLocation = 3;

	glBindAttribLocation(effectGameplayPresentProgram, vertexCoordLocation, "vertex");
	glBindAttribLocation(effectGameplayPresentProgram, textureCoordLocation, "textureCoord");
	glBindAttribLocation(effectGameplayPresentProgram, colorLocation, "color");

	// Link shader program.
	if (linkShaderProgram(effectGameplayPresentProgram, shader.vertex, shader.fragment) == false)
	{
		this->log.error().format("Could not link %s shader.", SHADER_NAME.c_str());
		return false;
	}

	this->effectGameplayPresentMatrixUniform = glGetUniformLocation(effectGameplayPresentProgram, "viewProjectionMatrix");
	assert(this->effectGameplayPresentMatrixUniform >= 0);

	this->effectGameplayPresentRenderUniform = glGetUniformLocation(effectGameplayPresentProgram, "renderTexture");
	assert(this->effectGameplayPresentRenderUniform >= 0);

	this->effectGameplayPresentLightUniform = glGetUniformLocation(effectGameplayPresentProgram, "effectMapTexture");
	assert(this->effectGameplayPresentLightUniform >= 0);

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Error = %d", error);
	}

	this->renderData.bindShaderProgram(effectGameplayPresentProgram);

	glm::mat4x4 modelViewProjectionMatrix = glm::ortho(0.0f, (float) windowSize.x, 0.0f, (float) windowSize.y, -1.0f, 1.0f);

	glUniformMatrix4fv(this->effectGameplayPresentMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));

	glActiveTexture(GL_TEXTURE0);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->litGameplayPresentTexture);
	glUniform1i(this->effectGameplayPresentRenderUniform, 0);

	glActiveTexture(GL_TEXTURE1);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->effectMapTexture);
	glUniform1i(this->effectGameplayPresentLightUniform, 1);

	glActiveTexture(GL_TEXTURE0);

	glGenBuffers(1, &this->effectGameplayPresentVBO);
	this->renderData.bindBuffer(GL_ARRAY_BUFFER, this->effectGameplayPresentVBO);

	TextureQuad::VertexAttribute::positionAttribPointer(vertexCoordLocation);
	TextureQuad::VertexAttribute::textureCoordinateAttribPointer(textureCoordLocation);
	TextureQuad::VertexAttribute::colorAttribPointer(colorLocation);

	glEnableVertexAttribArray(vertexCoordLocation);
	glEnableVertexAttribArray(textureCoordLocation);
	glEnableVertexAttribArray(colorLocation);

	return true;
}



bool OpenGlRenderer::setupSpriteRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{

	// Bind shader attributes.
	const GLuint vertexCoordLocation = 0;
	const GLuint textureCoordLocation = 1;
	const GLuint colorLocation = 2;
	const GLuint lightLuminanceLocation = 3;

	const GLuint outputColorLocation = 0;
	const GLuint outputLightLuminanceLocation = 1;

	{
		const std::string SHADER_NAME = "sprite";

		// Create shaders.
		GLuint spriteShaderProgram = glCreateProgram();
		assert(spriteShaderProgram != 0);

		const VertexAndFragmentShader shader = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
		assert(shader.isValid());

		glBindAttribLocation(spriteShaderProgram, vertexCoordLocation, "vertex");
		glBindAttribLocation(spriteShaderProgram, textureCoordLocation, "textureCoord");
		glBindAttribLocation(spriteShaderProgram, colorLocation, "color");

		glBindFragDataLocation(spriteShaderProgram, outputColorLocation, "outputFragColor");

		// Link shader program.
		if (linkShaderProgram(spriteShaderProgram, shader.vertex, shader.fragment) == false)
		{
			this->log.error().format("Could not link %s shader.", SHADER_NAME.c_str());
			return false;
		}

		this->renderData.bindShaderProgram(spriteShaderProgram);

		this->shaders[SHADER_NAME].shaderProgram = spriteShaderProgram;

		this->shaders[SHADER_NAME].ViewProjectionMatrixUniform = glGetUniformLocation(spriteShaderProgram, "viewProjectionMatrix");;
		
		
		assert(this->shaders[SHADER_NAME].ViewProjectionMatrixUniform >= 0);

		this->shaders[SHADER_NAME].TextureUniform = glGetUniformLocation(spriteShaderProgram, "renderTexture");
		assert(this->shaders[SHADER_NAME].TextureUniform >= 0);
		glActiveTexture(GL_TEXTURE0);
		glUniform1i(this->shaders[SHADER_NAME].TextureUniform, 0);
	}

	{
		const std::string SHADER_NAME = "spriteLightLuminance";

		// Create shaders.
		GLuint spriteShaderProgram = glCreateProgram();
		assert(spriteShaderProgram != 0);

		const VertexAndFragmentShader shader = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
		assert(shader.isValid());

		// Bind shader attributes.
		const GLuint vertexCoordLocation = 0;
		const GLuint textureCoordLocation = 1;
		const GLuint colorLocation = 2;
		const GLuint lightLuminanceLocation = 3;

		glBindAttribLocation(spriteShaderProgram, vertexCoordLocation, "vertex");
		glBindAttribLocation(spriteShaderProgram, textureCoordLocation, "textureCoord");
		glBindAttribLocation(spriteShaderProgram, colorLocation, "color");
		glBindAttribLocation(spriteShaderProgram, lightLuminanceLocation, "lightLuminance");

		glBindFragDataLocation(spriteShaderProgram, outputColorLocation, "outputFragColor");
		glBindFragDataLocation(spriteShaderProgram, outputLightLuminanceLocation, "outLightLuminance");

		// Link shader program.
		if (linkShaderProgram(spriteShaderProgram, shader.vertex, shader.fragment) == false)
		{
			this->log.error().format("Could not link %s shader.", SHADER_NAME.c_str());
			return false;
		}

		this->renderData.bindShaderProgram(spriteShaderProgram);

		this->shaders[SHADER_NAME].shaderProgram = spriteShaderProgram;

		this->shaders[SHADER_NAME].ViewProjectionMatrixUniform = glGetUniformLocation(spriteShaderProgram, "viewProjectionMatrix");;


		assert(this->shaders[SHADER_NAME].ViewProjectionMatrixUniform >= 0);

		this->shaders[SHADER_NAME].TextureUniform = glGetUniformLocation(spriteShaderProgram, "renderTexture");
		assert(this->shaders[SHADER_NAME].TextureUniform >= 0);
		glActiveTexture(GL_TEXTURE0);
		glUniform1i(this->shaders[SHADER_NAME].TextureUniform, 0);
	}
	
	this->textureRenderer.init(this->renderData, vertexCoordLocation, textureCoordLocation, colorLocation, lightLuminanceLocation);
	this->textureRenderer.enableCull(true);

	return true;
}

bool OpenGlRenderer::setupDebugRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	const std::string SHADER_NAME = "debug";

	// Create shaders.
	GLuint debugShaderProgram = glCreateProgram();
	assert(debugShaderProgram != 0);

	const VertexAndFragmentShader shader = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
	assert(shader.isValid());

	// Bind shader attributes.
	GLuint vertexLocation = 0;
	GLuint colorLocation = 1;
	glBindAttribLocation(debugShaderProgram, vertexLocation, "vertex");
	glBindAttribLocation(debugShaderProgram, colorLocation, "color");

	// Link shader program.
	if (linkShaderProgram(debugShaderProgram, shader.vertex, shader.fragment) == false)
	{
		this->log.error().format("Could not link %s shader.", SHADER_NAME.c_str());
		return false;
	}


	this->renderData.bindShaderProgram(debugShaderProgram);
	this->shaders[SHADER_NAME].shaderProgram = debugShaderProgram;
	this->shaders[SHADER_NAME].ViewProjectionMatrixUniform = glGetUniformLocation(debugShaderProgram, "modelViewProjectionMatrix");
	assert(this->shaders[SHADER_NAME].ViewProjectionMatrixUniform >= 0);


    this->debugRenderer->init(this->renderData, this->shaders[SHADER_NAME]);

	return true;
}

void OpenGlRenderer::addDebugGeometrySet(const std::shared_ptr<GeometrySet>& set)
{
	std::lock_guard<std::mutex> debugLock(this->debugRenderMutex);
	this->debugRenderer->addGeometrySet(set);
}

void OpenGlRenderer::removeDebugGeometrySet(const std::shared_ptr<GeometrySet>& set)
{
	std::lock_guard<std::mutex> debugLock(this->debugRenderMutex);
	this->debugRenderer->removeGeometrySet(set);
}

void OpenGlRenderer::addLight(std::shared_ptr<Light> light)
{
	std::lock_guard<std::mutex> lightLock(this->lightRenderMutex);

	if (light->isEffect() == true)
	{
		this->lightEffects.push_back(light);
		this->effectsUpdated = true;
	}
	else
	{
		if (light->renderHint == Light::RenderHint::DYNAMIC)
		{
			this->dynamicLights.push_back(light);
			this->dynamicLightsUpdated = true;
		}
		else if (light->renderHint == Light::RenderHint::STATIC)
		{
			this->staticLights.push_back(light);
			this->staticLightsUpdated = true;
		}
	}
}

void OpenGlRenderer::removeLight(std::shared_ptr<Light> light)
{
	std::lock_guard<std::mutex> lightLock(this->lightRenderMutex);


	if (light->isEffect() == true)
	{
		this->effectsUpdated = util::removeFast(this->lightEffects, light);
	}
	else
	{
		if (light->renderHint == Light::RenderHint::DYNAMIC)
		{
			this->dynamicLightsUpdated = util::removeFast(this->dynamicLights, light);
		}
		else if (light->renderHint == Light::RenderHint::STATIC)
		{
			this->staticLightsUpdated = util::removeFast(this->staticLights, light);
		}
	}
}

void OpenGlRenderer::resizeWindow(const glm::uvec2& windowSize)
{
	this->windowSize = windowSize;

	glViewport(0, 0, (GLsizei)this->windowSize.x, (GLsizei)this->windowSize.y);
	this->windowSizeChanged = true;

	deleteTextureIfValid(this->renderTexture);
	deleteTextureIfValid(this->lightLuminanceTexture);
	deleteTextureIfValid(this->effectMapTexture);
	deleteTextureIfValid(this->lightMapTexture);
	deleteTextureIfValid(this->lightHorizontalBlurTexture);
	deleteTextureIfValid(this->lightVerticalBlurTexture);
	deleteTextureIfValid(this->litGameplayPresentTexture);

	const glm::vec2 windowSizeFloat(this->windowSize.x, this->windowSize.y);

	glm::ivec2 supersampledSize;
	supersampledSize.x = static_cast<int>(windowSizeFloat.x * this->superSampleMultiplier);
	supersampledSize.y = static_cast<int>(windowSizeFloat.y * this->superSampleMultiplier);

	this->renderTexture = createTexture(supersampledSize.x, supersampledSize.y);
	this->lightLuminanceTexture = createTexture(supersampledSize.x, supersampledSize.y, GL_RG, GL_RG);
	this->gameplayFBO = createFrameBufferObject(supersampledSize.x, supersampledSize.y, {this->renderTexture, this->lightLuminanceTexture}, true);

	const GLsizei effectTextureWitdh = glm::max(1, static_cast<GLsizei>(windowSizeFloat.x * this->effectTextureSizeMultiplier));
	const GLsizei effectTextureHeight = glm::max(1, static_cast<GLsizei>(windowSizeFloat.y * this->effectTextureSizeMultiplier));

	const GLsizei lightTextureWidth = glm::max(1, static_cast<GLsizei>(windowSizeFloat.x * this->lightTextureSizeMultiplier));
	const GLsizei lightTextureHeight = glm::max(1, static_cast<GLsizei>(windowSizeFloat.y * this->lightTextureSizeMultiplier));

	this->effectMapTexture = createTexture(effectTextureWitdh, effectTextureHeight);
	this->effectMapFBO = createFrameBufferObject(effectTextureWitdh, effectTextureHeight, {this->effectMapTexture});

	this->lightMapTexture = createTexture(lightTextureWidth, lightTextureHeight);
	this->lightMapFBO = createFrameBufferObject(lightTextureWidth, lightTextureHeight, {this->lightMapTexture});
	
	this->lightHorizontalBlurTexture = createTexture(lightTextureWidth, lightTextureHeight);
	this->lightHorizontalBlurFBO = createFrameBufferObject(lightTextureWidth, lightTextureHeight, {this->lightHorizontalBlurTexture});
	
	this->lightVerticalBlurTexture = createTexture(lightTextureWidth, lightTextureHeight);
	this->lightVerticalBlurFBO = createFrameBufferObject(lightTextureWidth, lightTextureHeight, {this->lightVerticalBlurTexture});

	this->litGameplayPresentTexture = createTexture(static_cast<GLsizei>(this->windowSize.x), static_cast<GLsizei>(this->windowSize.y));
	this->litGameplayPresentFBO = createFrameBufferObject(static_cast<GLsizei>(this->windowSize.x), static_cast<GLsizei>(this->windowSize.y), {this->litGameplayPresentTexture}, true);

	if (this->blurEnabled == true)
	{
		this->lightTexture = this->lightVerticalBlurTexture;
	}
	else
	{
		this->lightTexture = this->lightMapTexture;
	}

	if (this->lightMapMultisamplingSamples > 0)
	{
		this->lightMapMultiSampleFBO = createMultisampleFrameBufferObject(static_cast<GLsizei>(lightTextureWidth), static_cast<GLsizei>(lightTextureHeight), std::vector<GLuint>(), static_cast<GLsizei>(this->lightMapMultisamplingSamples));
	}

	for (auto& subRenderer : this->subRenderers)
	{
		subRenderer->onScreenSizeChange(this->windowSize.x, this->windowSize.y);
	}

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Got error %d while resizing.", error);
	}
}

SubRenderer* OpenGlRenderer::addSubRenderer(std::unique_ptr<SubRenderer> renderer)
{
	SubRenderer* subRenderer = renderer.get();
	this->subRenderers.push_back(std::move(renderer));
	return subRenderer;
}

void OpenGlRenderer::removeSubRenderer(SubRenderer* renderer)
{
	auto renderIt = std::find_if(
			this->subRenderers.begin(),
			this->subRenderers.end(),
			[renderer] (const std::unique_ptr<SubRenderer>& renderToCheck)
			{
				return (renderer == renderToCheck.get());
			}
	);

	if (renderIt != this->subRenderers.end())
	{
		this->subRenderers.erase(renderIt);
	}
}

void OpenGlRenderer::addStenciledTiledTextureLayer(
	const std::string& layerName,
	std::unique_ptr<StenciledTiledTextureGenerator> generator,
	std::unique_ptr<StenciledTiledTextureRenderer> renderer,
	unsigned int renderLevel)
{
	const auto layerIt = this->stenciledTiledTextureLayers.find(layerName);

	if (layerIt == this->stenciledTiledTextureLayers.end())
	{
		StenciledTiledTextureLayer& layer = this->stenciledTiledTextureLayers[layerName];
		layer.generator = std::move(generator);
		layer.renderer = std::move(renderer);
		layer.renderLevel = renderLevel;

		layer.renderer->setup(this->renderData);
	}
}

void OpenGlRenderer::removeTiledTextureLayer(const std::string& layerName)
{
	this->stenciledTiledTextureLayers.erase(layerName);
}

void OpenGlRenderer::setBackgroundGradient(std::unique_ptr<BackgroundGradient> background)
{
	this->backgroundGradient = std::move(background);

	if (this->backgroundGradient != nullptr)
	{
		this->backgroundGradient->setupRendering(this->renderData, this->shaders["backgroundGradient"].shaderProgram);
	}
}

void OpenGlRenderer::lightUpdate(const std::shared_ptr<Light>& light)
{
	if (light->isEffect() == true)
	{
		this->effectsUpdated = true;
	}
	else
	{
		if (light->renderHint == Light::RenderHint::DYNAMIC)
		{
			this->dynamicLightsUpdated = true;
		}
		else if (light->renderHint == Light::RenderHint::STATIC)
		{
			this->staticLightsUpdated = true;
		}
	}
}

void OpenGlRenderer::setAmbientLightLevel(const float lightLevel)
{
	this->lightLevel = lightLevel;
}

bool OpenGlRenderer::setupLightMapRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	const std::string SHADER_NAME = "lightMap";

	GLuint lightMapShaderProgram = glCreateProgram();
	assert(lightMapShaderProgram != 0);

	const VertexAndFragmentShader shader = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
	assert(shader.isValid());

	// Bind shader attributes.
	GLuint vertexLocation = 0;
	GLuint colorLocation = 1;
	glBindAttribLocation(lightMapShaderProgram, vertexLocation, "vertex");
	glBindAttribLocation(lightMapShaderProgram, colorLocation, "color");

	// Link shader program.
	if (linkShaderProgram(lightMapShaderProgram, shader.vertex, shader.fragment) == false)
	{
		this->log.error().format("Could not link %s shader.", SHADER_NAME.c_str());
		return false;
	}

	GLint matrixUniformLocation = glGetUniformLocation(lightMapShaderProgram, "modelViewProjectionMatrix");
	assert(matrixUniformLocation >= 0);
	this->shaders[SHADER_NAME].ViewProjectionMatrixUniform = matrixUniformLocation;

	this->shaders[SHADER_NAME].shaderProgram = lightMapShaderProgram;

	glGenVertexArrays(1, &this->lightMapVAO);
	this->renderData.bindVertexArray(this->lightMapVAO);

	glGenBuffers(1, &this->lightMapVBO);
	this->renderData.bindBuffer(GL_ARRAY_BUFFER, this->lightMapVBO);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
			  sizeof(ObjectCoordinate),
			  (void*)offsetof(ObjectCoordinate, geometryVertex));

	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE,
			  sizeof(ObjectCoordinate),
			  (void*)offsetof(ObjectCoordinate, color));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	return true;
}

bool OpenGlRenderer::setupEffectMapRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	const std::string SHADER_NAME = "effectMap";

	// Create shaders.2
	GLuint effectMapShaderProgram = glCreateProgram();
	assert(effectMapShaderProgram != 0);

	const VertexAndFragmentShader shader = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
	assert(shader.isValid());

	// Bind shader attributes.
	GLuint vertexLocation = 0;
	GLuint colorLocation = 1;
	glBindAttribLocation(effectMapShaderProgram, vertexLocation, "vertex");
	glBindAttribLocation(effectMapShaderProgram, colorLocation, "color");

	// Link shader program.
	if (linkShaderProgram(effectMapShaderProgram, shader.vertex, shader.fragment) == false)
	{
		this->log.error().format("Could not link %s shader.", SHADER_NAME.c_str());
		return false;
	}

	GLint matrixUniformLocation = glGetUniformLocation(effectMapShaderProgram, "modelViewProjectionMatrix");
	assert(matrixUniformLocation >= 0);
	this->shaders[SHADER_NAME].ViewProjectionMatrixUniform = matrixUniformLocation;

	this->shaders[SHADER_NAME].shaderProgram = effectMapShaderProgram;


	glGenVertexArrays(1, &this->effectMapVAO);
	this->renderData.bindVertexArray(this->effectMapVAO);


	glGenBuffers(1, &this->effectMapVBO);
	this->renderData.bindBuffer(GL_ARRAY_BUFFER, this->effectMapVBO);


	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
			      sizeof(ObjectCoordinate),
			      (void*)offsetof(ObjectCoordinate, geometryVertex));

	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE,
			      sizeof(ObjectCoordinate),
			      (void*)offsetof(ObjectCoordinate, color));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	return true;
}

bool OpenGlRenderer::setupLightHorizontalBlur(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	glGenVertexArrays(1, &lightHorizontalBlurVAO);
	this->renderData.bindVertexArray(lightHorizontalBlurVAO);

	const std::string SHADER_NAME = "horizontalBlurCoordCalculation";

	// Create shaders.
	this->lightHorizontalBlurProgram = glCreateProgram();

	const GLuint vertexShader = this->createShader(resourceCache, shaderDirectory + SHADER_NAME + ".vert", GL_VERTEX_SHADER);
	assert(vertexShader != 0);

	const GLuint fragmentShader = this->createShader(resourceCache, shaderDirectory + "gaussianBlurShader.frag", GL_FRAGMENT_SHADER);
	assert(fragmentShader != 0);

	assert(this->lightHorizontalBlurProgram != 0);

	// Bind shader attributes.
	const GLuint vertexCoordLocation = 0;
	const GLuint textureCoordLocation = 1;

	glBindAttribLocation(lightHorizontalBlurProgram, vertexCoordLocation, "vertex");
	glBindAttribLocation(lightHorizontalBlurProgram, textureCoordLocation, "textureCoord");

	// Link shader program.
	if (linkShaderProgram(lightHorizontalBlurProgram, vertexShader, fragmentShader) == false)
	{
		this->log.error().format("Could not link %s shader.", SHADER_NAME.c_str());
		return false;
	}

	this->lightHorizontalBlurMatrixUniform = glGetUniformLocation(lightHorizontalBlurProgram, "viewProjectionMatrix");
	assert(this->lightHorizontalBlurMatrixUniform >= 0);

	this->lightHorizontalBlurTextureUniform = glGetUniformLocation(lightHorizontalBlurProgram, "renderTexture");
	assert(this->lightHorizontalBlurTextureUniform >= 0);

	this->lightHorizontalBlurSizeUniform = glGetUniformLocation(lightHorizontalBlurProgram, "blurSize");
	assert(this->lightHorizontalBlurSizeUniform >= 0);

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Error = %d", error);
	}
	
	this->renderData.bindShaderProgram(lightHorizontalBlurProgram);

	glm::mat4x4 modelViewProjectionMatrix = glm::ortho(0.0f, (float) windowSize.x, 0.0f, (float) windowSize.y, -1.0f, 1.0f);

	glUniformMatrix4fv(this->lightHorizontalBlurMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));

	float blurSize = this->pixelsPerBlurStep / (static_cast<float>(this->windowSize.x) * this->lightTextureSizeMultiplier);
	glUniform1f(this->lightHorizontalBlurSizeUniform, blurSize);

	glActiveTexture(GL_TEXTURE0);
	this->renderData.bindTexture(GL_TEXTURE_2D, lightMapTexture);
	glUniform1i(this->lightHorizontalBlurTextureUniform, 0);
		
	glGenBuffers(1, &this->lightHorizontalBlurVBO);
	this->renderData.bindBuffer(GL_ARRAY_BUFFER, this->lightHorizontalBlurVBO);
	
	TextureQuad::VertexAttribute::positionAttribPointer(vertexCoordLocation);
	TextureQuad::VertexAttribute::textureCoordinateAttribPointer(textureCoordLocation);

	glEnableVertexAttribArray(vertexCoordLocation);
	glEnableVertexAttribArray(textureCoordLocation);
	
	return true;
}

bool OpenGlRenderer::setupLightVerticalBlur(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{

	glGenVertexArrays(1, &lightVerticalBlurVAO);
	this->renderData.bindVertexArray(lightVerticalBlurVAO);
	
	const std::string SHADER_NAME = "verticalBlurCoordCalculation";

	// Create shaders.
	lightVerticalBlurProgram = glCreateProgram();
	assert(lightVerticalBlurProgram != 0);

	const GLuint vertexShader = this->createShader(resourceCache, shaderDirectory + SHADER_NAME + ".vert", GL_VERTEX_SHADER);
	assert(vertexShader != 0);

	const GLuint fragmentShader = this->createShader(resourceCache, shaderDirectory + "gaussianBlurShader.frag", GL_FRAGMENT_SHADER);
	assert(fragmentShader != 0);
	
	// Bind shader attributes.
	const GLuint vertexCoordLocation = 0;
	const GLuint textureCoordLocation = 1;

	glBindAttribLocation(lightVerticalBlurProgram, vertexCoordLocation, "vertex");
	glBindAttribLocation(lightVerticalBlurProgram, textureCoordLocation, "textureCoord");

	// Link shader program.
	if (linkShaderProgram(lightVerticalBlurProgram, vertexShader, fragmentShader) == false)
	{
		this->log.error().format("Could not link %s shader.", SHADER_NAME.c_str());
		return false;
	}

	this->lightVerticalBlurMatrixUniform = glGetUniformLocation(lightVerticalBlurProgram, "viewProjectionMatrix");
	assert(this->lightVerticalBlurMatrixUniform >= 0);

	this->lightVerticalBlurTextureUniform = glGetUniformLocation(lightVerticalBlurProgram, "renderTexture");
	assert(this->lightVerticalBlurTextureUniform >= 0);

	this->lightVerticalBlurSizeUniform = glGetUniformLocation(lightVerticalBlurProgram, "blurSize");
	assert(this->lightVerticalBlurSizeUniform >= 0);

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Error = %d", error);
	}
	
	this->renderData.bindShaderProgram(lightVerticalBlurProgram);
	
	glm::mat4x4 modelViewProjectionMatrix = glm::ortho(0.0f, (float) windowSize.x, 0.0f, (float) windowSize.y, -1.0f, 1.0f);

	glUniformMatrix4fv(this->lightVerticalBlurMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));

	float blurSize = this->pixelsPerBlurStep / (static_cast<float>(this->windowSize.y) * this->lightTextureSizeMultiplier);
	glUniform1f(this->lightVerticalBlurSizeUniform, blurSize);

	glActiveTexture(GL_TEXTURE0);
	this->renderData.bindTexture(GL_TEXTURE_2D, lightHorizontalBlurTexture);
	glUniform1i(this->lightVerticalBlurTextureUniform, 0);
	
	glGenBuffers(1, &this->lightVerticalBlurVBO);
	this->renderData.bindBuffer(GL_ARRAY_BUFFER, this->lightVerticalBlurVBO);
	
	TextureQuad::VertexAttribute::positionAttribPointer(vertexCoordLocation);
	TextureQuad::VertexAttribute::textureCoordinateAttribPointer(textureCoordLocation);

	glEnableVertexAttribArray(vertexCoordLocation);
	glEnableVertexAttribArray(textureCoordLocation);
	
	return true;
}

bool OpenGlRenderer::setupBackgroundGradientRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	const std::string SHADER_NAME = "backgroundGradient";

	// Create shaders.
	GLuint shaderProgram = glCreateProgram();
	assert(shaderProgram != 0);

	const VertexAndFragmentShader shader = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
	assert(shader.isValid());

	// Bind shader attributes.
	GLuint vertexLocation = 0;
	GLuint colorLocation = 1;
	glBindAttribLocation(shaderProgram, vertexLocation, "vertex");
	glBindAttribLocation(shaderProgram, colorLocation, "color");

	// Link shader program.
	if (linkShaderProgram(shaderProgram, shader.vertex, shader.fragment) == false)
	{
		this->log.error().format("Could not link %s shader.", SHADER_NAME.c_str());
		return false;
	}

	this->shaders[SHADER_NAME].shaderProgram = shaderProgram;

	return true;
}


void OpenGlRenderer::onLightMapRender()
{
	if (lightMapMultisamplingSamples > 0)
	{
		this->lightMapMultiSampleFBO.bind(GL_FRAMEBUFFER);
	}
	else
	{
		this->lightMapFBO.bind(GL_FRAMEBUFFER);
	}

	if (this->lightClearColor != this->currentClearColor)
	{
		glClearColor(lightClearColor.r, lightClearColor.g, lightClearColor.b, lightClearColor.a);
		this->currentClearColor = this->lightClearColor;
	}
	glClear(GL_COLOR_BUFFER_BIT);

	const GLsizei lightTextureWidth(static_cast<GLsizei>(static_cast<float>(this->windowSize.x) * this->lightTextureSizeMultiplier));
	const GLsizei lightTextureHeight(static_cast<GLsizei>(static_cast<float>(this->windowSize.y) * this->lightTextureSizeMultiplier));

	if (totalLightCoords > 0)
	{
		this->renderData.disable(GL_STENCIL_TEST);
		this->renderData.bindVertexArray(this->lightMapVAO);
		this->renderData.bindShaderProgram(this->shaders["lightMap"].shaderProgram);

		glUniformMatrix4fv(this->shaders["lightMap"].ViewProjectionMatrixUniform,
			1, GL_FALSE, glm::value_ptr(this->renderCamera->getViewProjectionMatrix()));

		if (this->lightTextureSizeMultiplier != 1.0f)
		{
			// We need to set the viewport to fit the new resolution.
			glViewport(0, 0, lightTextureWidth, lightTextureHeight);
		}

		glDrawArrays(GL_TRIANGLES, 0, (GLsizei)totalLightCoords);

		if (this->lightTextureSizeMultiplier != 1.0f)
		{
			glViewport(0, 0, static_cast<GLsizei>(this->windowSize.x), static_cast<GLsizei>(this->windowSize.y));
		}
	}

	if (lightMapMultisamplingSamples > 0)
	{
		this->lightMapMultiSampleFBO.bind(GL_READ_FRAMEBUFFER);
		this->lightMapFBO.bind(GL_DRAW_FRAMEBUFFER);
		glBlitFramebuffer(0, 0,
				  lightTextureWidth,
				  lightTextureHeight,
				  0, 0,
				  lightTextureWidth,
				  lightTextureHeight,
				  GL_COLOR_BUFFER_BIT, GL_LINEAR);
	}


	if (this->blurEnabled == true)
	{

#ifdef TIME_BLUR
		if (this->frameCount >= NUM_AVERAGING_FRAMES)
		{
			auto nanosecondsTotal =std::chrono::duration_cast<std::chrono::nanoseconds>(this->accumulatedBlurTime).count();
			auto nanosecondsPerFrame = nanosecondsTotal / this->frameCount;

			auto microsecondsTotal = std::chrono::duration_cast<std::chrono::microseconds>(this->accumulatedBlurTime).count();
			auto microsecondsPerFrame = microsecondsTotal / this->frameCount;

			auto millisecondsTotal = std::chrono::duration_cast<std::chrono::milliseconds>(this->accumulatedBlurTime).count();
			auto millisecondsPerFrame = millisecondsTotal / this->frameCount;

			this->log.debug().format("Blur rendering used %i nanoseconds per frame.", nanosecondsPerFrame);
			this->log.debug().format("Blur rendering used %i microseconds per frame.", microsecondsPerFrame);
			this->log.debug().format("Blur rendering used %i milliseconds per frame.", millisecondsPerFrame);

			this->frameCount = 0;
			this->accumulatedBlurTime = std::chrono::high_resolution_clock::duration(0);
		}

		auto startTime = std::chrono::high_resolution_clock::now();
#endif


		///////////// INSERT BLUR ////////
		this->renderData.disable(GL_BLEND);
		this->renderLightHorizontalBlur();
		this->renderLightVerticalBlur();
		this->renderData.enable(GL_BLEND);
		//////////////////////////////////

#ifdef TIME_BLUR
		auto endTime = std::chrono::high_resolution_clock::now();
		this->accumulatedBlurTime += (endTime - startTime);
		this->frameCount++;
#endif
	}
}

void OpenGlRenderer::onEffectMapRender()
{
	this->effectMapFBO.bind(GL_FRAMEBUFFER);

	if (this->lightClearColor != this->currentClearColor)
	{
		glClearColor(lightClearColor.r, lightClearColor.g, lightClearColor.b, lightClearColor.a);
		this->currentClearColor = this->lightClearColor;
	}
	glClear(GL_COLOR_BUFFER_BIT);

	if (totalEffectCoords > 0)
	{
		renderData.disable(GL_STENCIL_TEST);
		this->renderData.bindVertexArray(this->effectMapVAO);
		this->renderData.bindShaderProgram(this->shaders["effectMap"].shaderProgram);

		glUniformMatrix4fv(this->shaders["effectMap"].ViewProjectionMatrixUniform,
				   1, GL_FALSE, glm::value_ptr(this->renderCamera->getViewProjectionMatrix()));

		if (this->effectTextureSizeMultiplier != 1.0f)
		{
			const GLsizei effectTextureWitdh(static_cast<GLsizei>(static_cast<float>(this->windowSize.x) * this->effectTextureSizeMultiplier));
			const GLsizei effectTextureHeight(static_cast<GLsizei>(static_cast<float>(this->windowSize.y) * this->effectTextureSizeMultiplier));

			// We need to set the viewport to fit the new resolution.
			glViewport(0, 0, effectTextureWitdh, effectTextureHeight);
		}

		glDrawArrays(GL_TRIANGLES, 0, (GLsizei)totalEffectCoords);

		if (this->effectTextureSizeMultiplier != 1.0f)
		{
			glViewport(0, 0, static_cast<GLsizei>(this->windowSize.x), static_cast<GLsizei>(this->windowSize.y));
		}
	}
}

TextureRenderer& OpenGlRenderer::getTextureRenderer()
{
	return this->textureRenderer;
}


bool OpenGlRenderer::isDebugRenderingEnabled()
{
	return this->debugRenderingEnabled;
}


void OpenGlRenderer::lightRenderingDataPrepare()
{
	if (this->staticLightsUpdated == true)
	{
		this->numberOfStaticLightCoords = 0;
		this->staticLightCoords.clear();

		std::unique_lock<std::mutex> lightLock(this->lightRenderMutex);

		for (const std::shared_ptr<Light>& light : this->staticLights)
		{
			if (light != nullptr && light->isEnabled() == true)
			{
				GeometrySet& geometrySet = light->lightArea;

				if (geometrySet.isActive() == true && geometrySet.getGeometryPrimitive() == GeometryPrimitive::TRIANGLE)
				{
					unsigned int renderDataSize = 0;

					const std::vector<ObjectCoordinate>& setCoordinates = geometrySet.getVertexData();

					if (geometrySet.getDataMode() == GeometrySet::DataMode::REQUEST)
					{
						renderDataSize = geometrySet.updateRenderData();
					}
					else
					{
						renderDataSize = static_cast<unsigned int>(setCoordinates.size());
					}

					numberOfStaticLightCoords += renderDataSize;

					staticLightCoords.insert(staticLightCoords.end(), setCoordinates.begin(), setCoordinates.begin() + renderDataSize);
				}
			}
		}

		this->staticLightsUpdated = false;

		lightLock.unlock();
	}

	if (this->dynamicLightsUpdated == true)
	{
		this->numberOfDynamicLightCoords = 0;
		this->dynamicLightCoords.clear();

		std::unique_lock<std::mutex> lightLock(this->lightRenderMutex);

		for (const std::shared_ptr<Light>& light : this->dynamicLights)
		{
			if (light != nullptr && light->isEnabled() == true)
			{
				GeometrySet& geometrySet = light->lightArea;

				if (geometrySet.isActive() == true && geometrySet.getGeometryPrimitive() == GeometryPrimitive::TRIANGLE)
				{
					unsigned int renderDataSize = 0;

					const std::vector<ObjectCoordinate>& setCoordinates = geometrySet.getVertexData();

					if (geometrySet.getDataMode() == GeometrySet::DataMode::REQUEST)
					{
						renderDataSize = geometrySet.updateRenderData();
					}
					else
					{
						renderDataSize = static_cast<unsigned int>(setCoordinates.size());
					}

					numberOfDynamicLightCoords += renderDataSize;

					dynamicLightCoords.insert(dynamicLightCoords.end(), setCoordinates.begin(), setCoordinates.begin() + renderDataSize);
				}
			}
		}

		this->dynamicLightsUpdated = false;

		lightLock.unlock();

	}

	this->totalLightCoords = (GLsizei)(staticLightCoords.size() + dynamicLightCoords.size());
}

void OpenGlRenderer::lightRenderingIo()
{
	if (this->totalLightCoords > 0)
	{
		const GLsizeiptr staticLightDataSize = static_cast<GLsizeiptr>(this->staticLightCoords.size() * sizeof(ObjectCoordinate));
		const GLsizeiptr dynamicLightDataSize = static_cast<GLsizeiptr>(this->dynamicLightCoords.size() * sizeof(ObjectCoordinate));
		const GLsizeiptr totalDataSize = static_cast<GLsizeiptr>(static_cast<unsigned int>(this->totalLightCoords) * sizeof(ObjectCoordinate));

		this->renderData.bindBuffer(GL_ARRAY_BUFFER, this->lightMapVBO);

		if (this->lightDataSize < totalDataSize)
		{
			this->lightDataSize = 2 * totalDataSize;
			glBufferData(GL_ARRAY_BUFFER, this->lightDataSize, NULL, GL_STREAM_DRAW);
		}
		else if (totalDataSize < this->lightDataSize / 4)
		{
			this->lightDataSize = 2 * totalDataSize;
			glBufferData(GL_ARRAY_BUFFER, this->lightDataSize, NULL, GL_STREAM_DRAW);
		}

		handleAsyncDataCopy(staticLightCoords.data(), staticLightDataSize, dynamicLightCoords.data(), dynamicLightDataSize);
	}
}


void OpenGlRenderer::effectRenderingDataPrepare()
{
	if (this->effectsUpdated == true)
	{
		this->numberOfEffectCoords = 0;
		this->effectCoords.clear();

		std::unique_lock<std::mutex> lightLock(this->lightRenderMutex);

		for (const std::shared_ptr<Light>& light : this->lightEffects)
		{
			if (light != nullptr && light->isEnabled() == true)
			{
				GeometrySet& geometrySet = light->lightArea;

				if (geometrySet.isActive() == true && geometrySet.getGeometryPrimitive() == GeometryPrimitive::TRIANGLE)
				{
					unsigned int renderDataSize = 0;

					const std::vector<ObjectCoordinate>& setCoordinates = geometrySet.getVertexData();

					if (geometrySet.getDataMode() == GeometrySet::DataMode::REQUEST)
					{
						renderDataSize = geometrySet.updateRenderData();
					}
					else
					{
						renderDataSize = static_cast<unsigned int>(setCoordinates.size());
					}

					this->numberOfEffectCoords += renderDataSize;

					this->effectCoords.insert(effectCoords.end(), setCoordinates.begin(), setCoordinates.begin() + renderDataSize);
				}
			}
		}

		lightLock.unlock();
	}

	this->totalEffectCoords = GLsizei(this->effectCoords.size());
}

void OpenGlRenderer::effectRenderingIo()
{
	if (this->totalEffectCoords > 0)
	{
		const GLsizeiptr totalDataSize = this->totalEffectCoords * static_cast<GLsizeiptr>(sizeof(ObjectCoordinate));

		this->renderData.bindBuffer(GL_ARRAY_BUFFER, this->effectMapVBO);

		if (this->effectDataSize < totalDataSize)
		{
			this->effectDataSize = 2 * totalDataSize;
			glBufferData(GL_ARRAY_BUFFER, effectDataSize, NULL, GL_STREAM_DRAW);
		}
		else if (totalDataSize < this->effectDataSize / 4)
		{
			this->effectDataSize = 2 * totalDataSize;
			glBufferData(GL_ARRAY_BUFFER, effectDataSize, NULL, GL_STREAM_DRAW);
		}

		handleAsyncDataCopy(effectCoords.data(), totalDataSize);

		this->effectsUpdated = false;
	}
}

void OpenGlRenderer::debugDataPreparation()
{
	std::unique_lock<std::mutex> debugLock(this->debugRenderMutex, std::defer_lock);

	while (!this->quitDebugDataPreparation)
	{
		this->debugDataPreparationStartBarrier.wait();
		debugLock.lock();
		this->debugRenderer->prepareData();
		debugLock.unlock();
		this->debugDataPreparationFinishedBarrier.wait();
	}
}

void OpenGlRenderer::textureDataPreparation()
{
	while (!this->quitTextureDataPreparation)
	{
		this->textureDataPreparationStartBarrier.wait();
		this->parseSceneGraph();
		this->textureDataPreparationFinishedBarrier.wait();
	}
}

void OpenGlRenderer::lightingDataPreparation()
{
	while (!this->quitLightingDataPreparation)
	{
		this->lightingDataPreparationStartBarrier.wait();
		this->lightRenderingDataPrepare();
		this->lightingDataPreparationFinishedBarrier.wait();
	}
}

void OpenGlRenderer::effectDataPreparation()
{
	while (!this->quitEffectDataPreparation)
	{
		this->effectDataPreparationStartBarrier.wait();
		this->effectRenderingDataPrepare();
		this->effectDataPreparationFinishedBarrier.wait();
	}
}

void OpenGlRenderer::setLightStartRenderLevel(const unsigned int litRenderLevel)
{
	this->lightStartRenderLevel = litRenderLevel;
}

void OpenGlRenderer::subRenderersDataPreparation()
{
	while (!this->quitSubRenderersDataPreparation)
	{
		this->subRenderersDataPreparationStartBarrier.wait();
		for (auto& subRenderer : this->subRenderers)
		{
			subRenderer->prepareRendering(this->renderData);
		}
		this->subRenderersDataPreparationFinishedBarrier.wait();
	}
}

void OpenGlRenderer::organizeRenderSteps()
{
	this->renderSteps.clear();

	std::multimap<unsigned int, StenciledTiledTextureRenderer*> sortedStenciledTextureLayers;
	for (const auto& tileLayer : this->stenciledTiledTextureLayers)
	{
		sortedStenciledTextureLayers.insert({tileLayer.second.renderLevel, tileLayer.second.renderer.get()});
	}

	const ShaderInfo& litTextureShader = this->shaders["spriteLightLuminance"];
	const ShaderInfo& textureShader = this->shaders["sprite"];

	std::vector<std::unique_ptr<StenciledTiledTextureStencilStep>> stencilRenderSteps;

	unsigned int previousLayer = 0;
	GLint currentStencilRef = 1;

	if (this->backgroundGradient != nullptr)
	{
		std::unique_ptr<BackgroundGradientRenderStep> backgroundStep(new BackgroundGradientRenderStep(
			this->backgroundGradient.get(),
			previousLayer,
			currentStencilRef));

		this->renderSteps.push_back(std::move(backgroundStep));
	}

	for (const auto& layer : sortedStenciledTextureLayers)
	{
		const unsigned int currentLayer = layer.first;

		if (currentLayer > previousLayer && previousLayer < this->lightStartRenderLevel)
		{
			std::unique_ptr<TextureRenderStep> textureStep(new TextureRenderStep(
				this->textureRenderer,
				previousLayer,
				this->lightStartRenderLevel - 1u,
				textureShader.shaderProgram,
				currentStencilRef,
				false));

			this->renderSteps.push_back(std::move(textureStep));

			previousLayer = this->lightStartRenderLevel;
		}

		if (currentLayer > previousLayer)
		{
			std::unique_ptr<TextureRenderStep> litTextureStep(new TextureRenderStep(
				this->textureRenderer,
				previousLayer,
				currentLayer - 1u,
				litTextureShader.shaderProgram,
				currentStencilRef,
				true));

			this->renderSteps.push_back(std::move(litTextureStep));
		}

		std::unique_ptr<StenciledTiledTextureStencilStep> tiledStencilStep(new StenciledTiledTextureStencilStep(
			layer.second,
			layer.first,
			currentStencilRef));

		stencilRenderSteps.push_back(std::move(tiledStencilStep));

		std::unique_ptr<StenciledTiledTextureRenderStep> tiledRenderStep(new StenciledTiledTextureRenderStep(
			layer.second,
			layer.first,
			litTextureShader.shaderProgram,
			currentStencilRef));

		this->renderSteps.push_back(std::move(tiledRenderStep));

		currentStencilRef += 1;

		previousLayer = currentLayer;
	}

	const unsigned int lastRenderLevel = this->NUM_RENDER_LEVELS - 1;

	if (previousLayer < lastRenderLevel && previousLayer < this->lightStartRenderLevel)
	{
		std::unique_ptr<TextureRenderStep> textureStep(new TextureRenderStep(
			this->textureRenderer,
			previousLayer,
			this->lightStartRenderLevel - 1u,
			textureShader.shaderProgram,
			currentStencilRef,
			false));

		this->renderSteps.push_back(std::move(textureStep));

		previousLayer = this->lightStartRenderLevel;
	}

	if (previousLayer < lastRenderLevel)
	{
		std::unique_ptr<TextureRenderStep> litTextureStep(new TextureRenderStep(
			this->textureRenderer,
			previousLayer,
			lastRenderLevel,
			litTextureShader.shaderProgram,
			currentStencilRef,
			true));

		this->renderSteps.push_back(std::move(litTextureStep));
	}

	// Do the stencil operations before any rendering.
	this->renderSteps.insert(
		this->renderSteps.begin(),
		std::make_move_iterator(stencilRenderSteps.begin()),
		std::make_move_iterator(stencilRenderSteps.end()));
}

OpenGlRenderer::RenderStep::RenderStep(const unsigned int startLevel, const unsigned int endLevel):
	startLevel(startLevel),
	endLevel(endLevel)
{
}

OpenGlRenderer::RenderStep::~RenderStep() = default;

unsigned int OpenGlRenderer::RenderStep::getStartRenderLevel() const
{
	return this->startLevel;
}

unsigned int OpenGlRenderer::RenderStep::getEndRenderLevel() const
{
	return this->endLevel;
}

OpenGlRenderer::TextureRenderStep::TextureRenderStep(
		TextureRenderer& renderer,
		const unsigned int startLevel,
		const unsigned int endLevel,
		const GLuint shader,
		const GLint stencilRef,
		const bool affectedByLight):
	RenderStep(startLevel, endLevel),
	renderer(renderer),
	shader(shader),
	stencilRef(stencilRef),
	affectedByLight(affectedByLight)
{
}

void OpenGlRenderer::TextureRenderStep::render(RenderData& renderData, const glm::mat4& /*viewProjectionMatrix*/)
{
	if (this->affectedByLight == false)
	{
		glDrawBuffers(1, COLOR_ATTACHMENTS);
	}

	renderData.enable(GL_STENCIL_TEST);
	renderData.setStencilMask(0x00);
	renderData.setStencilFunc(GL_GREATER, this->stencilRef, 0xFF);

	renderData.bindShaderProgram(this->shader);
	this->renderer.drawData(renderData, this->getStartRenderLevel(), this->getEndRenderLevel());

	if (this->affectedByLight == false)
	{
		glDrawBuffers(2, COLOR_ATTACHMENTS);
	}
}

OpenGlRenderer::StenciledTiledTextureStencilStep::StenciledTiledTextureStencilStep(StenciledTiledTextureRenderer* renderer, const unsigned int renderLevel, const GLint ref) :
	RenderStep(renderLevel, renderLevel),
	renderer(renderer),
	ref(ref)
{
}

void OpenGlRenderer::StenciledTiledTextureStencilStep::render(RenderData& renderData, const glm::mat4& viewProjectionMatrix)
{
	assert(this->renderer != nullptr);

	this->renderer->stencil(renderData, viewProjectionMatrix, this->ref);
}

OpenGlRenderer::StenciledTiledTextureRenderStep::StenciledTiledTextureRenderStep(StenciledTiledTextureRenderer* renderer, const unsigned int renderLevel, const GLuint shader, const GLint ref):
	RenderStep(renderLevel, renderLevel),
	renderer(renderer),
	shader(shader),
	ref(ref)
{
}

void OpenGlRenderer::StenciledTiledTextureRenderStep::render(RenderData& renderData, const glm::mat4& viewProjectionMatrix)
{
	assert(this->renderer != nullptr);

	renderData.bindShaderProgram(this->shader);
	this->renderer->render(renderData, viewProjectionMatrix, this->ref, GL_EQUAL);
}

OpenGlRenderer::BackgroundGradientRenderStep::BackgroundGradientRenderStep(BackgroundGradient* gradient, const unsigned int renderLevel, const GLint stencilRef):
	RenderStep(renderLevel, renderLevel),
	gradient(gradient),
	stencilRef(stencilRef)
{
}

void OpenGlRenderer::BackgroundGradientRenderStep::render(RenderData& renderData, const glm::mat4& /*viewProjectionMatrix*/)
{
	glDrawBuffers(1, COLOR_ATTACHMENTS);

	renderData.enable(GL_STENCIL_TEST);
	renderData.setStencilMask(0x00);
	renderData.setStencilFunc(GL_GREATER, this->stencilRef, 0xFF);

	this->gradient->render(renderData);

	glDrawBuffers(2, COLOR_ATTACHMENTS);
}

OpenGlRenderer::StenciledTiledTextureLayer::StenciledTiledTextureLayer():
	renderLevel(0)
{
}

OpenGlRenderer::StenciledTiledTextureLayer::~StenciledTiledTextureLayer()
{
}

GLuint OpenGlRenderer::createShader(resource::IResourceAccess* resourceCache, const std::string& shaderPath, const GLenum shaderType) const
{
	const auto shaderHandle = resourceCache->getHandle(resource::Descriptor(shaderPath));

	if (!shaderHandle)
	{
		this->log.error().format("Could not open shader resource \"%s\"", shaderPath.c_str());
		return 0;
	}

	std::istringstream shaderStream(std::string(shaderHandle->getResourceBuffer(), shaderHandle->getResourceBufferSize()));

	auto shader = createShaderAutoVersion(shaderStream, shaderType, this->renderData.getGlMajorVersion(), this->renderData.getGlMinorVersion());

	if (shader.isCreated() == false)
	{
		this->log.error().format("Could not create shader \"%s\"", shaderPath.c_str());
		return 0;
	}
	else if (shader.isCompiled() == false)
	{
		const std::string errorString = shader.getCompileLog();

		if (errorString.empty() == false)
		{
			this->log.error().format("Could not compile shader \"%s\": %s", shaderPath.c_str(), errorString.c_str());
		}
		else
		{
			this->log.error().format("Could not compile shader \"%s\": Unknown reason.", shaderPath.c_str());
		}

		shader.destroy();
	}

	return shader.getId();
}

OpenGlRenderer::VertexAndFragmentShader OpenGlRenderer::createVertexAndFragmentShader(resource::IResourceAccess* resourceCache, const std::string& shaderPath) const
{
	VertexAndFragmentShader shader;
	shader.vertex = this->createShader(resourceCache, shaderPath + ".vert", GL_VERTEX_SHADER);
	shader.fragment = this->createShader(resourceCache, shaderPath + ".frag", GL_FRAGMENT_SHADER);

	return shader;
}

bool OpenGlRenderer::VertexAndFragmentShader::isValid() const
{
	return (this->vertex > 0 && this->fragment > 0);
}

namespace
{

std::vector<TextureQuad::VertexAttribute> makeTriangleStripBox(const math::Box<glm::vec2>& box)
{
	std::vector<TextureQuad::VertexAttribute> coords(4);

	coords[0].setPosition(box.getLowerLeft());
	coords[0].setTextureCoordinate({0.0f, 0.0f});

	coords[1].setPosition(box.getLowerRight());
	coords[1].setTextureCoordinate({1.0f, 0.0f});

	coords[2].setPosition(box.getUpperLeft());
	coords[2].setTextureCoordinate({0.0f, 1.0f});

	coords[3].setPosition(box.getUpperRight());
	coords[3].setTextureCoordinate({1.0f, 1.0f});

	return coords;
}

}

} } }
