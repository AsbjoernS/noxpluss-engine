/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/opengl/TextureRenderer.h>
#include <nox/app/graphics/opengl/opengl_utils.h>
#include <nox/app/graphics/render_utils.h>
#include <nox/app/graphics/opengl/RenderData.h>

#include <algorithm>
#include <cassert>
#include <cstring>
#include <glm/gtx/rotate_vector.hpp>

namespace nox { namespace app
{
namespace graphics
{

TextureRenderer::TextureRenderer(unsigned int numRenderLevels):
	NUM_RENDER_LEVELS(numRenderLevels),
	handleIdIncrementor(0),
	numberOfRenderQuads(0),
	textureRenderVao(0),
	indexBuffer(0),
	vertexBuffer(0),
	vertexDataChanged(false),
	dataSizeChanged(false),
	indexBufferDataSize(0),
	vertexBufferDataSize(0),
	cullEnabled(false),
	cullRotation(0.0f)
{
	assert(this->NUM_RENDER_LEVELS >= 1);
}

TextureRenderer::~TextureRenderer()
{
	if (this->textureRenderVao != 0)
	{
		glDeleteVertexArrays(1, &this->textureRenderVao);
	}

	if (this->indexBuffer != 0)
	{
		glDeleteBuffers(1, &this->indexBuffer);
	}

	if (this->vertexBuffer != 0)
	{
		glDeleteBuffers(1, &this->vertexBuffer);
	}
}

void TextureRenderer::init(RenderData& renderData, GLuint vertexAttribLocation, GLuint textureAttribLocation, GLuint colorLocation, GLuint lightLuminanceLocation)
{
	glGenVertexArrays(1, &this->textureRenderVao);
	glGenBuffers(1, &this->indexBuffer);
	glGenBuffers(1, &this->vertexBuffer);

	assert(
		this->indexBuffer > 0
		&& this->textureRenderVao > 0
		&& this->vertexBuffer > 0
	);

	renderData.bindVertexArray(this->textureRenderVao);
	renderData.bindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->indexBuffer);
	renderData.bindBuffer(GL_ARRAY_BUFFER, this->vertexBuffer);

	TextureQuad::VertexAttribute::positionAttribPointer(vertexAttribLocation);
	TextureQuad::VertexAttribute::textureCoordinateAttribPointer(textureAttribLocation);
	TextureQuad::VertexAttribute::colorAttribPointer(colorLocation);
	TextureQuad::VertexAttribute::luminanceAttribPointer(lightLuminanceLocation);

	glEnableVertexAttribArray(vertexAttribLocation);
	glEnableVertexAttribArray(textureAttribLocation);
	glEnableVertexAttribArray(colorLocation);
	glEnableVertexAttribArray(lightLuminanceLocation);
}

TextureRenderer::DataHandle TextureRenderer::requestDataSpace(unsigned int numQuads, unsigned int renderLevel)
{
	assert(renderLevel < this->NUM_RENDER_LEVELS);

	DataHandle handle(this->handleIdIncrementor);
	this->handleIdIncrementor++;

	TextureRenderData data;
	data.numQuads = numQuads;
	data.renderLevel = renderLevel;

	this->reserveSpace(data);

	this->handleData[handle] = data;

	auto dataIt = this->sortedRenderData.begin();

	while (dataIt != this->sortedRenderData.end() && dataIt->renderLevel <= data.renderLevel)
	{
		++dataIt;
	}

	this->sortedRenderData.insert(dataIt, data);

	return handle;
}

void TextureRenderer::reserveSpace(TextureRenderData& handleData)
{
	auto freeBlockIt = this->findFreeBlock(handleData);

	if (freeBlockIt != this->freeList.end())
	{
		handleData.startIndex = freeBlockIt->start;
		this->freeList.erase(freeBlockIt);
	}
	else
	{
		handleData.startIndex = this->textureQuads.size();
		this->textureQuads.resize(this->textureQuads.size() + handleData.numQuads);
		this->numberOfRenderQuads = this->textureQuads.size();
	}

	this->originalRenderLevels[handleData.renderLevel].dataSize += handleData.numQuads;
	this->renderLevels[handleData.renderLevel].dataSize = this->originalRenderLevels[handleData.renderLevel].dataSize;

	this->dataSizeChanged = true;
}

TextureRenderer::RenderIndicesIndex TextureRenderer::fillQuadRenderIndices(const TexturesIndex quadIndex, const RenderIndicesIndex startIndex)
{
	RenderIndicesIndex currentIndex = startIndex;
	const GLuint currentVertexIndex = static_cast<GLuint>(quadIndex * 4);

	this->renderIndices[currentIndex + 0] = currentVertexIndex + 0;
	this->renderIndices[currentIndex + 1] = currentVertexIndex + 1;
	this->renderIndices[currentIndex + 2] = currentVertexIndex + 2;
	this->renderIndices[currentIndex + 3] = currentVertexIndex + 0;
	this->renderIndices[currentIndex + 4] = currentVertexIndex + 2;
	this->renderIndices[currentIndex + 5] = currentVertexIndex + 3;

	currentIndex += this->INDICES_PER_QUAD;

	return currentIndex;
}

TextureRenderer::RenderIndicesIndex TextureRenderer::fillDataRenderIndices(const TextureRenderData& data, const RenderIndicesIndex startIndex)
{
	const TexturesIndex endQuadIndex = data.startIndex + data.numQuads;
	RenderIndicesIndex currentIndex = startIndex;

	for (TexturesIndex quadIndex = data.startIndex; quadIndex < endQuadIndex; quadIndex++)
	{
		currentIndex = this->fillQuadRenderIndices(quadIndex, currentIndex);
	}

	return currentIndex;
}

TextureRenderer::RenderIndicesIndex TextureRenderer::fillIndices()
{
	this->renderLevels = this->originalRenderLevels;

	RenderIndicesIndex currentIndex = 0;

	for (const TextureRenderData& data : this->sortedRenderData)
	{
		currentIndex = this->fillDataRenderIndices(data, currentIndex);
	}

	return currentIndex;
}

bool TextureRenderer::setData(const DataHandle& handle, const std::vector<TextureQuad::RenderQuad>& data)
{
	if (handle.isValid() == false || this->handleData.find(handle) == this->handleData.end())
	{
		return false;
	}

	const TextureRenderData& handleData = this->handleData[handle];

	assert(data.size() == this->handleData[handle].numQuads);

	this->fillRenderData(data, this->textureQuads, handleData);
	this->vertexDataChanged = true;

	return true;
}

void TextureRenderer::fillRenderData(const std::vector<TextureQuad::RenderQuad>& fillFrom, std::vector<TextureQuad::RenderQuad>& fillTo, const TextureRenderData& handleData)
{
	TexturesIndex dataSize = std::min(handleData.numQuads, fillFrom.size());
	TexturesIndex dataBegin = handleData.startIndex;

	for (TexturesIndex i = 0; i < dataSize; i++)
	{
		fillTo[dataBegin + i] = fillFrom[i];
	}
}

bool TextureRenderer::deleteData(DataHandle& handle)
{
	auto handleDataIt = this->handleData.find(handle);
	if (handleDataIt != this->handleData.end())
	{
		const TextureRenderData data = handleDataIt->second;

		this->handleData.erase(handle);
		this->sortedRenderData.remove(data);

		this->freeSpace(data);

		handle = DataHandle();

		return true;
	}
	else
	{
		return false;
	}
}

void TextureRenderer::freeSpace(const TextureRenderData& handleData)
{
	TextureBlock freeBlock;
	freeBlock.start = handleData.startIndex;
	freeBlock.length = handleData.numQuads;
	this->freeList.push_back(freeBlock);

	this->originalRenderLevels[handleData.renderLevel].dataSize -= handleData.numQuads;
	this->renderLevels[handleData.renderLevel].dataSize = this->originalRenderLevels[handleData.renderLevel].dataSize;

	this->dataSizeChanged = true;
	this->vertexDataChanged = true;
}

void TextureRenderer::drawData(RenderData& renderData, unsigned int startRenderLevel, unsigned int endRenderLevel)
{
	startRenderLevel = glm::clamp(startRenderLevel, 0u, this->NUM_RENDER_LEVELS);
	endRenderLevel = glm::clamp(endRenderLevel, 0u, this->NUM_RENDER_LEVELS);
	endRenderLevel = glm::max(startRenderLevel, endRenderLevel);

	renderData.bindVertexArray(this->textureRenderVao);

	const auto dataRange = this->getDataRange(startRenderLevel, endRenderLevel);

	const GLuint indexStart = static_cast<GLuint>(dataRange.first) * this->INDICES_PER_QUAD;
	const GLuint indexEnd = static_cast<GLuint>(dataRange.second) * this->INDICES_PER_QUAD;
	const GLsizei indexSize = static_cast<GLsizei>(indexEnd - indexStart);

	glDrawElements(GL_TRIANGLES, indexSize, GL_UNSIGNED_INT, reinterpret_cast<void*>(indexStart * sizeof(GLuint)));
}

void TextureRenderer::drawData(RenderData& renderData)
{
	this->drawData(renderData, 0, this->NUM_RENDER_LEVELS);
}

void TextureRenderer::handleIo(RenderData& renderData)
{
	if ((this->dataSizeChanged == true || this->cullEnabled == true) && this->numberOfRenderQuads > 0)
	{
		if (this->renderIndices.size() < this->textureQuads.size() * 6)
		{
			this->renderIndices.resize(this->textureQuads.size() * 6);
		}

		std::size_t numIndices = 0;

		if (this->cullEnabled == true)
		{
			numIndices = static_cast<std::size_t>(this->fillIndicesCulled());
		}
		else
		{
			numIndices = static_cast<std::size_t>(this->fillIndices());
		}

		renderData.bindVertexArray(this->textureRenderVao);

		GLsizeiptr dataSize = static_cast<GLsizeiptr>(numIndices * sizeof(GLuint));

		if (this->indexBufferDataSize < dataSize)
		{
			this->indexBufferDataSize = 2 * dataSize;
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indexBufferDataSize, nullptr, GL_STREAM_DRAW);
		}
		else if (dataSize < this->indexBufferDataSize / 4)
		{
			this->indexBufferDataSize = 2 * dataSize;
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indexBufferDataSize, nullptr, GL_STREAM_DRAW);
		}

		handleAsyncDataCopy(this->renderIndices.data(), dataSize, GL_MAP_INVALIDATE_BUFFER_BIT, GL_ELEMENT_ARRAY_BUFFER);

		this->dataSizeChanged = false;
	}

	if (this->vertexDataChanged == true && this->numberOfRenderQuads > 0)
	{
		renderData.bindVertexArray(this->textureRenderVao);
		renderData.bindBuffer(GL_ARRAY_BUFFER, this->vertexBuffer);

		GLsizeiptr dataSize = static_cast<GLsizeiptr>(this->numberOfRenderQuads * sizeof(TextureQuad::RenderQuad));

		if (this->vertexBufferDataSize < dataSize)
		{
			this->vertexBufferDataSize = 2 * dataSize;
			glBufferData(GL_ARRAY_BUFFER, this->vertexBufferDataSize, nullptr, GL_STREAM_DRAW);
		}
		else if (dataSize < this->vertexBufferDataSize / 4)
		{
			this->vertexBufferDataSize = 2 * dataSize;
			glBufferData(GL_ARRAY_BUFFER, this->vertexBufferDataSize, nullptr, GL_STREAM_DRAW);
		}

		handleAsyncDataCopy(this->textureQuads.data(), dataSize, GL_MAP_INVALIDATE_RANGE_BIT);

		this->vertexDataChanged = false;
	}
}

void TextureRenderer::enableCull(bool enable)
{
	this->cullEnabled = enable;
}

void TextureRenderer::setCullArea(const math::Box<glm::vec2>& area, const float rotation)
{
	this->cullArea = area;
	this->cullRotation = rotation;
}

TextureRenderer::RenderIndicesIndex TextureRenderer::fillIndicesCulled()
{
	this->renderLevels.clear();

	const glm::vec2 areaCenter = this->cullArea.getCenter();
	const math::Box<glm::vec2> localArea(this->cullArea.lowerBound - areaCenter, this->cullArea.upperBound - areaCenter);

	RenderIndicesIndex currentIndex = 0;

	for (const TextureRenderData& data : this->sortedRenderData)
	{
		for (TexturesIndex i = 0; i < data.numQuads; i++)
		{
			const TexturesIndex quadIndex = data.startIndex + i;
			const TextureQuad::RenderQuad& renderQuad = this->textureQuads[quadIndex];

			TextureQuad::RenderQuad localQuad;
			localQuad.bottomLeft.setPosition(glm::rotate(glm::vec2(renderQuad.bottomLeft.getPosition()) - areaCenter, -this->cullRotation));
			localQuad.bottomRight.setPosition(glm::rotate(glm::vec2(renderQuad.bottomRight.getPosition()) - areaCenter, -this->cullRotation));
			localQuad.topRight.setPosition(glm::rotate(glm::vec2(renderQuad.topRight.getPosition()) - areaCenter, -this->cullRotation));
			localQuad.topLeft.setPosition(glm::rotate(glm::vec2(renderQuad.topLeft.getPosition()) - areaCenter, -this->cullRotation));

			const math::Box<glm::vec2> quadBoundingBox = findBoundingBox(localQuad);

			if (localArea.intersects(quadBoundingBox) == true)
			{
				currentIndex = this->fillQuadRenderIndices(quadIndex, currentIndex);

				this->renderLevels[data.renderLevel].dataSize += 1;
			}
		}
	}

	const RenderIndicesIndex currentNumIndices = currentIndex;

	return currentNumIndices;
}

std::pair<TextureRenderer::TexturesIndex, TextureRenderer::TexturesIndex> TextureRenderer::getDataRange(const unsigned int startLevel, const unsigned int endLevel) const
{
	TexturesIndex start = 0;

	auto renderLevelIt = this->renderLevels.begin();

	while (renderLevelIt != this->renderLevels.end() && renderLevelIt->first < startLevel)
	{
		start += renderLevelIt->second.dataSize;
		renderLevelIt++;
	}

	TexturesIndex end = start;

	while (renderLevelIt != this->renderLevels.end() && renderLevelIt->first <= endLevel)
	{
		end += renderLevelIt->second.dataSize;
		renderLevelIt++;
	}

	return std::make_pair(start, end);
}

TextureRenderer::FreeListIterator TextureRenderer::findFreeBlock(const TextureRenderData& data)
{
	auto blockIt = this->freeList.begin();

	while (blockIt != this->freeList.end())
	{
		if (blockIt->length == data.numQuads)
		{
			return blockIt;
		}

		++blockIt;
	}

	return this->freeList.end();
}

TextureRenderer::DataHandle::DataHandle():
	id(-1)
{
}

TextureRenderer::DataHandle::DataHandle(unsigned int id):
	id(static_cast<int>(id))
{
}

TextureRenderer::DataHandle::DataHandle(const DataHandle& other):
	id(other.id)
{
}

bool TextureRenderer::DataHandle::operator==(const DataHandle& other) const
{
	return (this->id == other.id);
}

bool TextureRenderer::DataHandle::operator<(const DataHandle& other) const
{
	return (this->id < other.id);
}

bool TextureRenderer::DataHandle::isValid() const
{
	return (this->id >= 0);
}

TextureRenderer::DataHandle& TextureRenderer::DataHandle::operator=(const DataHandle& other)
{
	this->id = other.id;
	return *this;
}

bool TextureRenderer::TextureRenderData::operator<(const TextureRenderData& other) const
{
	return this->renderLevel < other.renderLevel;
}

bool TextureRenderer::TextureRenderData::operator==(const TextureRenderData& other) const
{
	return this->startIndex == other.startIndex;
}

}
} }
