/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/IContext.h>
#include <nox/app/graphics/opengl/DebugRenderer.h>
#include <nox/app/graphics/opengl/TiledTextureRenderer.h>
#include <nox/app/graphics/opengl/StenciledTiledTextureRenderer.h>
#include <nox/app/graphics/opengl/opengl_utils.h>
#include <nox/app/graphics/opengl/BackgroundGradient.h>
#include <nox/app/graphics/SubRenderer.h>
#include <nox/app/graphics/SceneGraphNode.h>
#include <nox/app/graphics/TransformationNode.h>
#include <nox/app/graphics/StenciledTiledTextureGenerator.h>
#include <nox/app/graphics/Geometry.h>
#include <nox/app/graphics/Light.h>
#include <nox/app/graphics/opengl/TrdOpenGlRenderer.h>
#include <nox/app/graphics/TrdCamera.h>
#include <nox/app/resource/Handle.h>
#include <json/reader.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <nox/app/resource/IResourceAccess.h>
#include <nox/util/algorithm.h>
#include <SDL2/SDL.h>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cstring>
#include <cassert>

#include <glm/gtx/transform.hpp>	// For glm::translate(..)

#include <nox/logic/physics/box2d/BulletDebugDraw.h>

namespace nox { namespace app { namespace graphics
{

namespace
{
	std::vector<TextureQuad::VertexAttribute> makeTriangleStripBox(const math::Box<glm::vec2>& box);
}

TrdOpenGlRenderer::TrdOpenGlRenderer() :
	renderCamera(std::make_shared<TrdCamera>(glm::uvec2(1, 1))),
	windowSizeChanged(false)
{
	debugRenderer = std::make_unique<nox::logic::physics::BulletDebugDraw>();
}

TrdOpenGlRenderer::~TrdOpenGlRenderer()
{
}

bool TrdOpenGlRenderer::init(IContext* context, const std::string& shaderDirectory, const glm::uvec2& windowSize)
{

	const std::string SHADER_NAME = "simple3d";
	const std::string DEBUG_SHADER_NAME = "debug3d";
	assert(context != nullptr);


	// Set up logger
	this->log = context->createLogger();
	this->log.setName("TrdOpenGLRenderer");

	// Get access to resource cache (for loading the shader files)
	resource::IResourceAccess* resourceCache = context->getResourceAccess();
	assert(resourceCache != nullptr);

	// Initialize OpenGL
	if (this->initOpenGL(windowSize) == false)
	{
		return false;
	}

	// Create shader program
	simple3Dprogram = glCreateProgram();
	debugRenderProgram = glCreateProgram();

	// Create the shaders
	const VertexAndFragmentShader shaders = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
	const VertexAndFragmentShader debugShaders = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + DEBUG_SHADER_NAME);
	assert(shaders.isValid());
	assert(debugShaders.isValid());

	// Bind attributes for the shader
	const GLuint vertexLocation = 0;
	//glBindAttribLocation(simple3Dprogram, vertexLocation, "vertexPosition");

	if (!linkShaderProgram(simple3Dprogram, shaders.vertex, shaders.fragment))
	{
		this->log.error().format("Could not link %s shader.", SHADER_NAME.c_str());
		return false;
	}
	if (!linkShaderProgram(debugRenderProgram, debugShaders.vertex, debugShaders.fragment))
	{
		this->log.error().format("Could not link %s shader.", DEBUG_SHADER_NAME.c_str());
		return false;
	}


	// TODO: move this to another place?
	mvpHandle = glGetUniformLocation(simple3Dprogram, "mvpMatrix");
	lightPosHandle = glGetUniformLocation(simple3Dprogram, "lightPos");
	cameraPosHandle = glGetUniformLocation(simple3Dprogram, "cameraPos");

	glBindAttribLocation(simple3Dprogram, 0, "vertex");
	glBindAttribLocation(simple3Dprogram, 1, "normal");
	glBindAttribLocation(simple3Dprogram, 2, "tangent");
	glBindAttribLocation(simple3Dprogram, 3, "color");
	glBindAttribLocation(simple3Dprogram, 4, "uv");

	return true;
}

void TrdOpenGlRenderer::setGraphicsAssetManager(const std::shared_ptr<TrdGraphicsAssetManager>& assetManager)
{
	this->assetManager = assetManager;
}


void TrdOpenGlRenderer::onRender()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glUniform3fv(lightPosHandle, 1, glm::value_ptr(renderCamera->getPosition()));
	glUniform3fv(cameraPosHandle, 1, glm::value_ptr(renderCamera->getPosition()));

	assert(rootSceneNode);
	glm::mat4x4 viewProjection = renderCamera->getViewProjectionMatrix();
	
	renderData.bindShaderProgram(this->simple3Dprogram);
	rootSceneNode->onTraverse(*assetManager, viewProjection, mvpHandle, /*TODO: remove*/ simple3Dprogram);

	// TODO: debugRenderer
	if (debugRenderer->getIsDebugModeEnabled())
	{
		renderData.bindShaderProgram(this->debugRenderProgram);
		debugRenderer->onDebugRender(renderData, viewProjection);
	}

	GLenum error = glGetError();
	while (error != GL_NO_ERROR)
	{
		this->log.error().format("OpenGL: %i", error);
		error = glGetError();
	}
}

void TrdOpenGlRenderer::setCamera(const std::shared_ptr<TrdCamera>& camera)
{
    this->renderCamera = camera;
}

void TrdOpenGlRenderer::setRootSceneNode(const std::shared_ptr<TrdTransformationNode>& rootSceneNode)
{
	this->rootSceneNode = rootSceneNode;
}

bool TrdOpenGlRenderer::toggleDebugRendering()
{
	this->debugRenderer->toggleDebugDraw();
	return this->debugRenderer->getIsDebugModeEnabled();
}

bool TrdOpenGlRenderer::initOpenGL(const glm::uvec2& windowSize)
{
	this->windowSize = windowSize;

	glewExperimental = GL_TRUE;
	GLenum glewError = glewInit();

	glGetError();
	if (glewError != GLEW_OK)
	{
		glewGetErrorString(glewError);
		this->log.error().format("Error initializing GLEW: %s", (const char *) glewGetErrorString(glewError));
		return false;
	}

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Error initializing OpenGL: %d", error);
		return false;
	}

	GLint glMajor = 0;
	GLint glMinor = 0;
	glGetIntegerv(GL_MAJOR_VERSION, &glMajor);
	glGetIntegerv(GL_MINOR_VERSION, &glMinor);

	this->renderData = RenderData(static_cast<GLuint>(glMajor), static_cast<GLuint>(glMinor), nullptr);

	// Draw only vertecies that are visible to the camera:
	renderData.enable(GL_DEPTH_TEST);
	renderData.depthFunc(GL_LESS);

	// Draw wireframe only:
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINES);		

	// Set background color
	glClearColor(0.2, 0.0, 0.2, 1.0);
	
	return true;
}

void TrdOpenGlRenderer::resizeWindow(const glm::uvec2& windowSize)
{
	this->windowSize = windowSize;

	glViewport(0, 0, (GLsizei)this->windowSize.x, (GLsizei)this->windowSize.y);

	this->windowSizeChanged = true;

	const glm::vec2 windowSizeFloat(this->windowSize.x, this->windowSize.y);

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Got error %d while resizing.", error);
	}
}

bool TrdOpenGlRenderer::isDebugRenderingEnabled()
{
	return this->debugRenderer->getIsDebugModeEnabled();
}

GLuint TrdOpenGlRenderer::createShader(resource::IResourceAccess* resourceCache, const std::string& shaderPath, const GLenum shaderType) const
{
	// Use resourcecache to get handle to the file
	const auto shaderHandle = resourceCache->getHandle(resource::Descriptor(shaderPath));

	if (!shaderHandle)
	{
		this->log.error().format("Could not open shader resource \"%s\"", shaderPath.c_str());
		return 0;
	}

	// Get the shader file content
	std::istringstream shaderStream(std::string(shaderHandle->getResourceBuffer(), shaderHandle->getResourceBufferSize()));

	// Create the shader, using the file content
	auto shader = createShaderAutoVersion(shaderStream, shaderType, 3, 0);

	if (shader.isCreated() == false)
	{
		this->log.error().format("Could not create shader \"%s\"", shaderPath.c_str());
		return 0;
	}
	else if (shader.isCompiled() == false)
	{
		const std::string errorString = shader.getCompileLog();

		if (errorString.empty() == false)
		{
			this->log.error().format("Could not compile shader \"%s\": %s", shaderPath.c_str(), errorString.c_str());
		}
		else
		{
			this->log.error().format("Could not compile shader \"%s\": Unknown reason.", shaderPath.c_str());
		}

		shader.destroy();
	}

	return shader.getId();
}

TrdOpenGlRenderer::VertexAndFragmentShader TrdOpenGlRenderer::createVertexAndFragmentShader(resource::IResourceAccess* resourceCache, const std::string& shaderPath) const
{
	VertexAndFragmentShader shader;
	shader.vertex = this->createShader(resourceCache, shaderPath + ".vert", GL_VERTEX_SHADER);
	shader.fragment = this->createShader(resourceCache, shaderPath + ".frag", GL_FRAGMENT_SHADER);

	return shader;
}

bool TrdOpenGlRenderer::VertexAndFragmentShader::isValid() const
{
	return (this->vertex > 0 && this->fragment > 0);
}

nox::logic::physics::BulletDebugDraw* TrdOpenGlRenderer::getDebugRenderer()
{
	return debugRenderer.get();
}


namespace
{

}

} } }
