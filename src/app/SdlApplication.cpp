/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/SdlApplication.h>

#include <nox/util/string_utils.h>

#include <SDL.h>

namespace nox { namespace app
{

SdlApplication::SdlApplication(const std::string& applicationName, const std::string& organizationName):
	Application(applicationName, organizationName)
{
}

SdlApplication::SdlApplication(SdlApplication&& other):
	Application(std::move(other)),
	log(std::move(other.log))
{
}

SdlApplication& SdlApplication::operator=(SdlApplication&& other)
{
	Application::operator=(std::move(other));

	this->log = std::move(other.log);

	return *this;
}

bool SdlApplication::onInit()
{
	this->log = this->createLogger();
	this->log.setName("SdlApplication");

	if (SDL_Init(SDL_INIT_EVENTS) != 0)
	{
		this->log.error().format("Failed initializing SDL: %s", SDL_GetError());
		return false;
	}

	this->log.verbose().raw("SDL initialized.");

	return true;
}

void SdlApplication::onDestroy()
{
	SDL_Quit();

	this->log.verbose().raw("SDL quit.");
}

bool SdlApplication::initializeSdlSubsystem(const Uint32 subsystemFlag)
{
	if (SDL_InitSubSystem(subsystemFlag) != 0)
	{
		this->log.error().format("Failed initializing subsystem %u: %s", subsystemFlag, SDL_GetError());
		return false;
	}

	this->log.verbose().format("SDL subsystem initialized: %u", subsystemFlag);

	return true;
}

void SdlApplication::destroySdlsubSystem(const Uint32 subsystemFlag)
{
	SDL_QuitSubSystem(subsystemFlag);

	this->log.verbose().format("SDL subsystem quit: %u", subsystemFlag);
}

void SdlApplication::onUpdate(const Duration& /*deltaTime*/)
{
	SDL_Event event;

	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_EventType::SDL_QUIT)
		{
			this->quitApplication();
		}
		else
		{
			this->onSdlEvent(event);
		}
	}
}

std::string SdlApplication::getStorageDirectoryPath(const bool allowSpaces) const
{
	std::string formattedOrganization;
	std::string formattedName;

	if (allowSpaces == false)
	{
		formattedOrganization = util::removeSpaces(this->getOrganizationName());
		formattedName = util::removeSpaces(this->getName());
	}
	else
	{
		formattedOrganization = this->getOrganizationName();
		formattedName = this->getName();
	}

	auto sdlPrefPath = SDL_GetPrefPath(formattedOrganization.c_str(), formattedName.c_str());

	std::string path;

	if (sdlPrefPath)
	{
		path.assign(sdlPrefPath);

		SDL_free(sdlPrefPath);
	}

	return path;
}

void SdlApplication::onSdlEvent(const SDL_Event& /*event*/)
{
}

} }
