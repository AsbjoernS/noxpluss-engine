


#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <nox/logic/actor/event/TrdTransformChange.h>

namespace nox {
	namespace logic {
		namespace actor
		{

			const event::Event::IdType TrdTransformChange::ID = "nox.actor.Trdtransform";

			TrdTransformChange::TrdTransformChange(Actor* transformedActor, const glm::vec3& position, const glm::vec3 rotation, const glm::vec3& scale) :
				Event(ID, transformedActor),
				position(position),
				scale(scale),
				rotation(rotation)
			{
				
			}

			

			TrdTransformChange::~TrdTransformChange() = default;

			const glm::vec3& TrdTransformChange::getPosition() const
			{
				return this->position;
			}

			const glm::vec3& TrdTransformChange::getScale() const
			{
				return this->scale;
			}

			const glm::vec3& TrdTransformChange::getRotation() const
			{
				return this->rotation;
			}

			glm::mat4 TrdTransformChange::getTransformMatrix() const
			{
				glm::mat4 positionMatrix = glm::translate(this->position);
				glm::mat4 scaleMatrix = glm::scale(this->scale);
				glm::mat4 rotationMatrix = glm::rotate(this->rotation.x, glm::vec3(1.0f, 0.0f, 0.0f)) *
					glm::rotate(this->rotation.y, glm::vec3(0.0f, 1.0f, 0.0f)) *
					glm::rotate(this->rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

				return positionMatrix * rotationMatrix * scaleMatrix;
			}

		}
	}
}
