/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/physics/box2d/ActorMotionState.h>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

namespace nox { namespace logic { namespace physics
{

ActorMotionState::ActorMotionState(actor::Actor* actor) 
{
	this->actorTransform = actor->findComponent<actor::TrdTransform>();
}

ActorMotionState::~ActorMotionState()
{
}

void ActorMotionState::getWorldTransform(btTransform& worldTrans) const
{
	btVector3 startPosition(actorTransform->getPosition().x, actorTransform->getPosition().y, actorTransform->getPosition().z);
	btQuaternion startRotation(actorTransform->getRotation().x, actorTransform->getRotation().y, actorTransform->getRotation().z);

	worldTrans.setOrigin(startPosition);
	worldTrans.setRotation(startRotation);
}

void ActorMotionState::setWorldTransform(const btTransform& worldTrans)
{
	glm::vec3 position(worldTrans.getOrigin().getX(), worldTrans.getOrigin().getY(), worldTrans.getOrigin().getZ());
	glm::quat rotation(worldTrans.getRotation().getX(), worldTrans.getRotation().getY(), worldTrans.getRotation().getZ(), worldTrans.getRotation().getW());
	glm::normalize(rotation);
	this->actorTransform->setPosition(position);
	this->actorTransform->setRotation(rotation);

}

} } }
