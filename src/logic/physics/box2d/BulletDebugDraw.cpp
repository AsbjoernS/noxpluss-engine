/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/physics/box2d/BulletDebugDraw.h>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>
#include <SDL_opengl.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace nox { namespace logic { namespace physics
{

BulletDebugDraw::BulletDebugDraw() :
	VAOInited(false),
	isDebugModeEnabled(false)
{
}

BulletDebugDraw::~BulletDebugDraw()
{
	glDeleteVertexArrays(1, &debugVAO);
	glDeleteBuffers(1, &debugVBO);
}

void BulletDebugDraw::drawLine(const btVector3& from, const btVector3& to, const btVector3& color)
{
	this->vertices.push_back(glm::vec3(from.getX(), from.getY(), from.getZ()));
	this->vertices.push_back(glm::vec3(to.getX(), to.getY(), to.getZ()));
}

void BulletDebugDraw::drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color)
{
}

void BulletDebugDraw::reportErrorWarning(const char* warningString)
{
}

void BulletDebugDraw::draw3dText(const btVector3& location, const char* textString)
{
}

void BulletDebugDraw::setDebugMode(int debugMode)
{
	this->debugMode = debugMode;
}

int BulletDebugDraw::getDebugMode() const
{
	return this->debugMode;
}

void BulletDebugDraw::toggleDebugDraw()
{
	this->isDebugModeEnabled = !this->isDebugModeEnabled;
}

bool BulletDebugDraw::getIsDebugModeEnabled()
{
	return this->isDebugModeEnabled;
}

void BulletDebugDraw::onDebugRender(nox::app::graphics::RenderData renderData, const glm::mat4x4& viewPorjection)
{
	if (vertices.empty() == false)
	{
		if (this->VAOInited == false)
		{
			glGenVertexArrays(1, &debugVAO);
			glGenBuffers(1, &debugVBO);

			mvpUniform = glGetUniformLocation(renderData.getBoundShaderProgram(), "mvpMatrix");

			this->VAOInited = true;
		}
		else
		{
			glBindVertexArray(debugVAO);
			glBindBuffer(GL_ARRAY_BUFFER, debugVBO);
			glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vertices[0]), &vertices[0], GL_DYNAMIC_DRAW);

			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
			glBindAttribLocation(renderData.getBoundShaderProgram(), 0, "vertex");

			glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, glm::value_ptr(viewPorjection));

			glDrawArrays(GL_LINES, 0, this->vertices.size() * 3);

			vertices.clear();
		}
	}
}

} } }
