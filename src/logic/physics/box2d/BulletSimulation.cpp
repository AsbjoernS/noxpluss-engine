/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/physics/box2d/BulletSimulation.h>

#include <nox/logic/physics/box2d/box2d_utils.h>
#include <nox/logic/physics/GravitationalForce.h>
#include <nox/logic/physics/physics_utils.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/TrdTransform.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/graphics/DebugGeometryChange.h>
#include <nox/logic/graphics/DebugRenderingEnabled.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/IContext.h>
#include <nox/app/IContext.h>
#include <nox/app/graphics/Geometry.h>
#include <nox/app/graphics/Light.h>
#include <nox/app/graphics/IRenderer.h>
#include <nox/util/boost_utils.h>
#include <nox/util/chrono_utils.h>
#include <nox/util/algorithm.h>
#include <nox/util/math/vector_math.h>
#include <nox/util/math/geometry.h>
#include <nox/util/math/angle.h>
#include <nox/logic/world/event/ActorCreated.h>
#include <nox/logic/physics/actor/TrdActorPhysics.h>


#include <poly2tri.h>
#include <Box2D/Common/b2Settings.h>

#include <BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>
#include <BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h>

#include <nox/logic/physics/box2d/ActorMotionState.h>

#include <iostream>
#include <unordered_map>
#include <vector>


namespace nox { namespace logic { namespace physics
{

BulletSimulation::BulletSimulation(IContext* logicContext) :
	listener("BulletSimulation")
{
	this->logicContext = logicContext;

	this->listener.setup(this, this->logicContext->getEventBroadcaster(), event::ListenerManager::StartListening_t());
	this->listener.addEventTypeToListenFor(nox::logic::world::ActorCreated::ID);

	this->broadphase = std::make_shared<btDbvtBroadphase>();

	this->collisionConfiguration = std::make_shared<btDefaultCollisionConfiguration>();
	this->dispatcher = std::make_shared<btCollisionDispatcher>(collisionConfiguration.get());
	btGImpactCollisionAlgorithm::registerAlgorithm(dispatcher.get());

	this->solver = std::make_shared<btSequentialImpulseConstraintSolver>();

	this->dynamicsWorld = std::make_shared <btDiscreteDynamicsWorld>(this->dispatcher.get(), this->broadphase.get(), this->solver.get(), this->collisionConfiguration.get());
	this->dynamicsWorld->setGravity(btVector3(0.0f, -9.81f, 0.0f));

	this->dynamicsWorld->setInternalTickCallback(myTickCallback);
	this->dynamicsWorld->setWorldUserInfo(this);
}

BulletSimulation::~BulletSimulation()
{
	for (const auto& trdActorData : this->actorMap)
	{
		btRigidBody* mainBody = trdActorData.second.mainBody;

		btCollisionShape* shape = mainBody->getCollisionShape();
		btMotionState* motionState = mainBody->getMotionState();

		delete shape;
		delete motionState;
		this->dynamicsWorld->removeRigidBody(mainBody);
		delete mainBody;
	}
}

void BulletSimulation::createActorBody(actor::Actor* actor, const  BulletBodyDefinition& bodyDefinition)
{
	if (this->actorMap.find(actor->getId()) != this->actorMap.end())
	{
		this->log.error().raw("Failed creating actor body. Actor already added to physics.");
		return;
	}

	//TODO: implement map
	btCollisionShape* shape;
	ActorMotionState* motionState;
	btRigidBody* rigidBody;
	btScalar mass = 0;
	btVector3 inertia(0, 0, 0);

	motionState = new ActorMotionState(actor);
	
	shape = createShape(bodyDefinition);

	if (shape != nullptr)
	{

		switch (bodyDefinition.bodyType)
		{
		case PhysicalBodyType::DYNAMIC:
			mass = bodyDefinition.mass;
			if (mass > 0)
			{
				shape->calculateLocalInertia(mass, inertia);
			}
			
			break;
		case PhysicalBodyType::KINEMATIC:

			//TODO
			break;

		default:
			//TODO add logger output here
			break;
		}
	}

	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, motionState, shape, inertia);
	rigidBody = new btRigidBody(fallRigidBodyCI);
	rigidBody->setCollisionFlags(rigidBody->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
	rigidBody->setDamping(bodyDefinition.linearDamping, bodyDefinition.angularDamping);

	rigidBody->setLinearVelocity(btVector3(bodyDefinition.linearVelocity.x, bodyDefinition.linearVelocity.y, bodyDefinition.linearVelocity.z));
	rigidBody->setAngularVelocity(btVector3(bodyDefinition.angularVelocity.x, bodyDefinition.angularVelocity.y, bodyDefinition.angularVelocity.z));
	rigidBody->setFriction(bodyDefinition.friction);
	rigidBody->setRestitution(bodyDefinition.restitution);

	TrdActorData& actorData = this->actorMap[actor->getId()];
	rigidBody->setUserPointer(&actorData);
	actorData.actor = actor;
	actorData.mainBody = rigidBody;
	this->dynamicsWorld->addRigidBody(rigidBody);

	rigidBody->setUserPointer(&actorData);

	rigidBody->getUserPointer();

	auto jj = static_cast<TrdActorData*>(rigidBody->getUserPointer());
}

btCollisionShape* BulletSimulation::createShape(const BulletBodyDefinition &bodyDefinition)
{
	btCollisionShape * shape = nullptr;
	
	switch (bodyDefinition.shape.type)
	{
	case TrdShapeType::NONE:
		shape = new btEmptyShape();
		break;
	case TrdShapeType::SPHERE:
		shape = new btSphereShape(bodyDefinition.shape.circleRadius);
		break;
	case TrdShapeType::BOX:
		shape = new btBoxShape(btVector3(bodyDefinition.shape.size.x / 2, bodyDefinition.shape.size.y / 2, bodyDefinition.shape.size.z / 2));
		break;
	case TrdShapeType::CYLINDER:
		shape = new btCylinderShape(btVector3(bodyDefinition.shape.size.x / 2, bodyDefinition.shape.size.y / 2, bodyDefinition.shape.size.z / 2));
		break;
	case TrdShapeType::CAPSULE:
		shape = new btCapsuleShape(bodyDefinition.shape.circleRadius, bodyDefinition.shape.height);
		break;
	case TrdShapeType::CONE:
		shape = new btConeShape(bodyDefinition.shape.circleRadius, bodyDefinition.shape.height);
		break;
	case TrdShapeType::PLANE:
		//ONLY static
		shape = new btStaticPlaneShape(btVector3(bodyDefinition.shape.planeOffset.x, bodyDefinition.shape.planeOffset.y, bodyDefinition.shape.planeOffset.z), bodyDefinition.shape.planeConstant);
		break;
	case TrdShapeType::CONVEXHULLSHAPE:
		//TODO: find a way to get a hold of Mesh
		//	shape = new btConvexHullShape(nullptr, numPoints, stride);		//http://bulletphysics.org/Bullet/BulletFull/classbtConvexHullShape.html#a069cf26ba277f9f5f141128fee345eaf
	case TrdShapeType::COMPOUNDSHAPE:
		shape = new btCompoundShape(bodyDefinition.shape.enableDynamicAabbTree);
		break;
	case TrdShapeType::GIMPACTTRIANGLEMESHSHAPE:
		//TODO: mesh
		break;
	case TrdShapeType::HEIGHTFIELD:
		//@PARAM: int heightStickWidth, int heightStickLength, const void *heightfieldData, btScalar heightScale, btScalar minHeight, btScalar maxHeight, int upAxis, PHY_ScalarType heightDataType, bool flipQuadEdges	
		//shape = new btHeightfieldTerrainShape();
		break;
	case TrdShapeType::SCALEDBVHTRIANGLE:
		//ONLY static
		//The btScaledBvhTriangleMeshShape allows to instance a scaled version of an existing btBvhTriangleMeshShape.
		//Note that each btBvhTriangleMeshShape still can have its own local scaling, independent from this btScaledBvhTriangleMeshShape 'localScaling'
		
		//shape = new btScaledBvhTriangleMeshShape();
		
		break;
	case TrdShapeType::BVHTRIANGLE:
		//ONLY static
		//shape = new btBvhTriangleMeshShape();
	default:
		//TODO add logger output here!!
		break;
	}

	

	return shape;
}

void BulletSimulation::setLogger(app::log::Logger logger)
{
	this->log = std::move(logger);
	this->log.setName("BulletSimulation");
}


void BulletSimulation::onUpdate(const Duration& deltaTime)
{
	if (this->debugRenderer->getIsDebugModeEnabled())
	{
		this->dynamicsWorld->debugDrawWorld();
	}
	this->dynamicsWorld->stepSimulation(util::durationToSeconds<float32>(deltaTime));
	//how many contact point caches there are:
	int numManifolds = dynamicsWorld->getDispatcher()->getNumManifolds();
	for (int i = 0; i<numManifolds; i++)
	{	//get the first contact point cache
		btPersistentManifold* contactManifold = dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
		
		//maintains all information that is needed for a collision detection: Shape, Transform and AABB proxy.
		btCollisionObject* obA = const_cast<btCollisionObject*>(contactManifold->getBody0());
		btCollisionObject* obB = const_cast<btCollisionObject*>(contactManifold->getBody1());
		//returns number of cached Points
		int numContacts = contactManifold->getNumContacts();
		for (int j = 0; j<numContacts; j++)
		{
			//ManifoldContactPoint collects and maintains persistent contactpoints.
			//used to improve stability and performance of rigidbody dynamics response.
			//retrieves the first one
			btManifoldPoint& pt = contactManifold->getContactPoint(j);
			//if crash?
			

				if (pt.getDistance() < 0.f)
				{
					const btVector3& ptA = pt.getPositionWorldOnA();
					const btVector3& ptB = pt.getPositionWorldOnB();
					const btVector3& normalOnB = pt.m_normalWorldOnB;
					//syncScene();
				}
			}
		
	}
}

void BulletSimulation::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{
	if (event->isType(nox::logic::world::ActorCreated::ID))
	{
		const auto actorCreated = static_cast<nox::logic::world::ActorCreated*>(event.get());
		const auto actorPhysics = actorCreated->getActor()->findComponent<nox::logic::physics::TrdActorPhysics>();
		if (actorPhysics != nullptr)
		{
			actorPhysics->onCreate();
		}
	}
}


void BulletSimulation::setDebugRenderer(BulletDebugDraw* debugRenderer)
{
	debugRenderer->setDebugMode(debugRenderer->DBG_MAX_DEBUG_DRAW_MODE);
	this->dynamicsWorld->setDebugDrawer(debugRenderer);
	this->debugRenderer = debugRenderer;
}

bool BulletSimulation::getDebugRenderingEnabled()
{
	return this->debugRenderer->getIsDebugModeEnabled();
}

void BulletSimulation::myTickCallback(btDynamicsWorld *world, btScalar timeStep) {
	//	printf(�The world just ticked by %f seconds\n�, (float)timeStep);
	assert(world);

	BulletSimulation * bulletSim = static_cast<BulletSimulation*>(world->getWorldUserInfo());
	CollisionPairs currentTickCollisionPairs;

	btDispatcher* dispatcher = world->getDispatcher();
	for (int i = 0; i < dispatcher->getNumManifolds(); i++)
	{
		const btPersistentManifold * manifold = dispatcher->getManifoldByIndexInternal(i);
		assert(manifold);
		if (!manifold)
		{
			continue;
		}
		const btRigidBody* bodyOne = static_cast<const btRigidBody*>(manifold->getBody0());
		const btRigidBody* bodyTwo = static_cast<const btRigidBody*>(manifold->getBody1());

		const bool swapped = bodyOne > bodyTwo;

		const btRigidBody* sortedBodyA = swapped ? bodyOne : bodyTwo;
		const btRigidBody* sortedBodyB = swapped ? bodyTwo : bodyOne;

		const CollisionPair pair = std::make_pair(sortedBodyA, sortedBodyB);
		currentTickCollisionPairs.insert(pair);
		if (bulletSim->oldTickCollisionPairs.find(pair) == bulletSim->oldTickCollisionPairs.end())
		{
			//new contact
			bulletSim->sendCollisionPairAddEvent(manifold, bodyOne, bodyTwo);
		}
	}
	CollisionPairs removedCollisionPairs;

	std::set_difference(bulletSim->oldTickCollisionPairs.begin(), bulletSim->oldTickCollisionPairs.end(), currentTickCollisionPairs.begin(), 
		currentTickCollisionPairs.end(), std::inserter(removedCollisionPairs, removedCollisionPairs.begin()));

	for (CollisionPairs::const_iterator it = removedCollisionPairs.begin(),  end = removedCollisionPairs.end(); it != end; ++it)
	{
		const btRigidBody* bodyOne = it->first;
		const btRigidBody* bodyTwo = it->second;

		bulletSim->sendCollisionPairRemoveEvent(bodyOne, bodyTwo);
	}
	bulletSim->oldTickCollisionPairs = currentTickCollisionPairs;
}


void BulletSimulation::sendCollisionPairAddEvent(const btPersistentManifold* manifold, const btRigidBody* bodyOne, const btRigidBody* bodyTwo)
{
	//if (bodyOne->getUserPointer() || bodyTwo->getUserPointer())
	//{
	//	// only triggers have non-NULL userPointers

	//	// figure out which actor is the trigger
	//	btRigidBody const * triggerBody, *otherBody;

	//	if (bodyOne->getUserPointer())
	//	{
	//		triggerBody = bodyOne;
	//		otherBody = bodyTwo;
	//	}
	//	else
	//	{
	//		otherBody = bodyOne;
	//		triggerBody = bodyTwo;
	//	}

	//	// send the trigger event.
	//	
	//}
	//else
	//{
		TrdActorData *actorOne = static_cast<TrdActorData*>(bodyOne->getUserPointer());
		TrdActorData *actorTwo = static_cast<TrdActorData*>(bodyOne->getUserPointer());

		nox::logic::actor::Identifier idOne = actorOne->actor->getId();
		nox::logic::actor::Identifier idTwo = actorTwo->actor->getId();
		
		if (!idOne.isValid() || !idTwo.isValid())
		{
			// something is colliding with a non-actor.  we currently don't send events for that
			return;
		}

		// this pair of colliding objects is new.  send a collision-begun event
		std::vector<glm::vec3> collisionPoints;
		glm::vec3 sumNormalForce;
		glm::vec3 sumFrictionForce;

		for (int pointIdx = 0; pointIdx < manifold->getNumContacts(); ++pointIdx)
		{
			btManifoldPoint const & point = manifold->getContactPoint(pointIdx);

			collisionPoints.push_back(btVector3_to_Vec3(point.getPositionWorldOnB()));

			sumNormalForce += btVector3_to_Vec3(point.m_combinedRestitution * point.m_normalWorldOnB);
			sumFrictionForce += btVector3_to_Vec3(point.m_combinedFriction * point.m_lateralFrictionDir1);
		}
		//actorOne->actor->broadCastComponentEvent()
		std::cout << "a collision started \n";
		// send the event for the game
		//shared_ptr<EvtData_PhysCollision> pEvent(GCC_NEW EvtData_PhysCollision(id0, id1, sumNormalForce, sumFrictionForce, collisionPoints));
		//IEventManager::Get()->VQueueEvent(pEvent);
	//}
}

void BulletSimulation::sendCollisionPairRemoveEvent(const btRigidBody* bodyOne, const btRigidBody* bodyTwo)
{
	//if (bodyOne->getUserPointer() || bodyTwo->getUserPointer())
	//{
	//	// figure out which actor is the trigger
	//	btRigidBody const * triggerBody, *otherBody;

	//	if (bodyOne->getUserPointer())
	//	{
	//		triggerBody = bodyOne;
	//		otherBody = bodyTwo;
	//	}
	//	else
	//	{
	//		otherBody = bodyOne;
	//		triggerBody = bodyTwo;
	//	}

	//	// send the trigger event.
	//	int const triggerId = *static_cast<int*>(triggerBody->getUserPointer());
	///*	shared_ptr<EvtData_PhysTrigger_Leave> pEvent(GCC_NEW EvtData_PhysTrigger_Leave(triggerId, FindActorID(otherBody)));
	//	IEventManager::Get()->VQueueEvent(pEvent);*/
	//}
	//else
	//{
		TrdActorData *actorOne = static_cast<TrdActorData*>(bodyOne->getUserPointer());
		TrdActorData *actorTwo = static_cast<TrdActorData*>(bodyTwo->getUserPointer());

		nox::logic::actor::Identifier idOne = actorOne->actor->getId();
		nox::logic::actor::Identifier idTwo = actorTwo->actor->getId();

		if (!idOne.isValid()  || !idTwo.isValid() )
		{
			// collision is ending between some object(s) that don't have actors.  we don't send events for that.
			return;
		}
		// send the event for the game
		std::cout << "a collision ended \n";
		//shared_ptr<EvtData_PhysSeparation> pEvent(GCC_NEW EvtData_PhysSeparation(id0, id1));
		//IEventManager::Get()->VQueueEvent(pEvent);
	//}
}

glm::vec3 BulletSimulation::btVector3_to_Vec3(btVector3 const & btvec)
{
	return glm::vec3(btvec.x(), btvec.y(), btvec.z());
}

} } }
