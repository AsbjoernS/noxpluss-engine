/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/physics/actor/TrdActorPhysics.h>

#include <nox/logic/IContext.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/TrdTransform.h>
#include <nox/logic/actor/event/TrdTransformChange.h>
#include <nox/util/json_utils.h>
#include <nox/util/math/angle.h>
#include <boost/geometry/strategies/strategies.hpp>
#include <boost/geometry/algorithms/area.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <nox/logic/physics/Simulation.h>

namespace nox { namespace logic { namespace physics
{

const TrdActorPhysics::IdType TrdActorPhysics::NAME = "TrdPhysics";

const TrdActorPhysics::IdType& TrdActorPhysics::getName() const
{
	return TrdActorPhysics::NAME;
}




bool TrdActorPhysics::initialize(const Json::Value& componentJsonObject)
{
	std::string shape = componentJsonObject.get("shape", "").asString();
	std::string type = componentJsonObject.get("type", "").asString();
	physicalProperties.shape.height = componentJsonObject.get("height", 0).asFloat();
	physicalProperties.shape.circleRadius = componentJsonObject.get("radius", 0).asFloat();
	physicalProperties.shape.size = util::parseJsonVec(componentJsonObject["size"], glm::vec3(0.0f, 0.0f, 0.0f));
	if (std::strcmp(shape.c_str(), "plane") == false)
	{
		physicalProperties.shape.type = TrdShapeType::PLANE;
		//TODO: read in offset for plane shape
	}
	else if (std::strcmp(shape.c_str(), "sphere") == false)
	{
		physicalProperties.shape.type = TrdShapeType::SPHERE;
	}
	else if (std::strcmp(shape.c_str(), "cone") == false)
	{
		physicalProperties.shape.type = TrdShapeType::CONE;
	}
	else if (std::strcmp(shape.c_str(), "box") == false)
	{
		physicalProperties.shape.type = TrdShapeType::BOX;
	}
	else if (std::strcmp(shape.c_str(), "capsule") == false)
	{
		physicalProperties.shape.type = TrdShapeType::CAPSULE;
	}
	else if (std::strcmp(shape.c_str(), "cylinder") == false)
	{
		physicalProperties.shape.type = TrdShapeType::CYLINDER;
	}

	

	if (std::strcmp(type.c_str(), "dynamic") == false)
	{
		this->physicalProperties.bodyType = PhysicalBodyType::DYNAMIC;
		physicalProperties.mass = componentJsonObject.get("mass", 1.0f).asFloat();
		physicalProperties.linearDamping = componentJsonObject.get("linearDamping", 0).asFloat();
		physicalProperties.angularDamping = componentJsonObject.get("angularDamping", 0).asFloat();
		physicalProperties.linearVelocity = util::parseJsonVec(componentJsonObject["linearVelocity"], glm::vec3(0, 0, 0));
		physicalProperties.angularVelocity = util::parseJsonVec(componentJsonObject["angularVelocity"], glm::vec3(0, 0, 0));
		physicalProperties.restitution = componentJsonObject.get("restitution", 0.0f).asFloat();
		physicalProperties.friction = componentJsonObject.get("friction", 0.0f).asFloat();
	}
	else if (std::strcmp(type.c_str(), "static") == false)
	{
		this->physicalProperties.bodyType = PhysicalBodyType::STATIC;
	}

	this->physics = this->getLogicContext()->getTrdPhysics();
	
	return true;
}
	 
void TrdActorPhysics::onCreate()
{
	auto transformComp = this->getOwner()->findComponent<actor::TrdTransform>();
	if (transformComp != nullptr)
	{
		this->physicalProperties.position = transformComp->getPosition();
	}
	physics->createActorBody(this->getOwner(), physicalProperties);
}
	 
	 
void TrdActorPhysics::onDestroy()
{

}
	 
	 
void TrdActorPhysics::onComponentEvent(const std::shared_ptr<event::Event>& event)
{

}
	 
	 
void TrdActorPhysics::onUpdate(const Duration& deltaTime)
{

}
	 
void TrdActorPhysics::onActivate()
{

}
	 
void TrdActorPhysics::onDeactivate()
{

}


void TrdActorPhysics::serialize(Json::Value& componentObject)
{
}


} } }
