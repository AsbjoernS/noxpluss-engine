/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/Logic.h>
#include <nox/app/IContext.h>
#include <nox/logic/View.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/Identifier.h>
#include <nox/logic/event/Manager.h>
#include <nox/logic/physics/Simulation.h>
#include <nox/logic/physics/TrdSimulation.h>
#include <nox/logic/world/Manager.h>
#include <cassert>

namespace nox { namespace logic
{

Logic::Logic():
	paused(true),
	eventManager(std::make_unique<event::Manager>())
{
}

Logic::~Logic() = default;

app::IContext* Logic::getApplicationContext() const NOX_NOEXCEPT
{
	assert(this->getContext() != nullptr);
	return this->getContext();
}

void Logic::addView(std::unique_ptr<View> view)
{
	view->initialize(this);
	this->viewContainer.push_back(std::move(view));
}

void Logic::pause(bool pause)
{
	if (this->paused != pause)
	{
		this->paused = pause;

		if (this->paused == true)
		{
			this->log.verbose().raw("Paused.");
		}
		else
		{
			this->log.verbose().raw("Unpaused.");
		}
	}
}

bool Logic::isPaused() const
{
	return this->paused;
}

event::IBroadcaster* Logic::getEventBroadcaster()
{
	return this->eventManager.get();
}

physics::Simulation* Logic::getPhysics()
{
	return this->physics.get();
}

physics::TrdSimulation* Logic::getTrdPhysics()
{
	return this->trdPhysics.get();
}

app::resource::IResourceAccess* Logic::getResourceAccess()
{
	return this->getApplicationContext()->getResourceAccess();
}

app::storage::IDataStorage* Logic::getDataStorage()
{
	return this->getApplicationContext()->getDataStorage();
}

app::log::Logger Logic::createLogger()
{
	return this->getApplicationContext()->createLogger();
}

const View* Logic::findControllingView(const actor::Identifier& actorId) const
{
	auto viewIt = this->viewContainer.cbegin();
	const View* view = nullptr;

	while (view == nullptr && viewIt != this->viewContainer.cend())
	{
		const auto controlledActor = (*viewIt)->getControlledActor();

		if (controlledActor && controlledActor->getId() != actorId)
		{
			view = viewIt->get();
		}

		++viewIt;
	}

	return view;
}

void Logic::setPhysics(std::unique_ptr<physics::Simulation> physics)
{
	this->physics = std::move(physics);
}

void Logic::setPhysics(std::unique_ptr<physics::TrdSimulation> physics)
{
	this->trdPhysics = std::move(physics);
}

void Logic::setWorldHandler(std::unique_ptr<world::Manager> worldHandler)
{
	this->world = std::move(worldHandler);
}

void Logic::destroy()
{
	for (const std::unique_ptr<View>& view : this->viewContainer)
	{
		view->destroy();
	}

	this->viewContainer.clear();

	this->log.verbose().raw("Destroyed.");
}

void Logic::onInit()
{
	this->log = this->getApplicationContext()->createLogger();
	this->log.setName("NoxLogic");

	this->log.verbose().raw("Initialized.");
}

void Logic::onUpdate(const Duration& deltaTime)
{
	for (const std::unique_ptr<View>& view : this->viewContainer)
	{
		view->update(deltaTime);
	}

	while (this->eventManager->hasQueuedEvents() == true)
	{
		this->eventManager->broadcastEvents();
	}

	if (this->physics != nullptr)
	{
		this->physics->onUpdate(deltaTime);
		this->physics->updateLighting();
	}

	if (this->world != nullptr)
	{
		this->world->onUpdate(deltaTime);
	}

	while (this->eventManager->hasQueuedEvents() == true)
	{
		this->eventManager->broadcastEvents();
	}
}

void Logic::onUpdateFinished(const Duration& alpha)
{
	if (this->physics != nullptr)
	{
		this->physics->onSyncState(alpha);
	}
}

void Logic::onSuccess()
{
	this->destroy();
}

void Logic::onFail()
{
	this->destroy();
}

void Logic::onAbort()
{
	this->destroy();
}

} }
