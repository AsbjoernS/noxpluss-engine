/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <glm/gtc/matrix_transform.hpp>
#include <nox/logic/graphics/event/TrdActorGraphicsCreated.h>

namespace nox { namespace logic { namespace actor
{

	const event::Event::IdType TrdActorGraphicsCreated::ID = "nox.logic.actor.trd_actor_graphics_created";

TrdActorGraphicsCreated::TrdActorGraphicsCreated(Actor* actorgraphicsCreated, std::string path, std::string name) :
	Event(ID, actorgraphicsCreated),
	path(path),
	name(name)
{
}

TrdActorGraphicsCreated::~TrdActorGraphicsCreated() = default;

const std::string TrdActorGraphicsCreated::getPath() const
{
	return this->path;
}

const std::string TrdActorGraphicsCreated::getName() const
{
	return this->name;
}

}
}
}
