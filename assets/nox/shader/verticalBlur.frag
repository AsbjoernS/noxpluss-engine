/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//? required 130

in vec2 fragTexCoord;

uniform sampler2D renderTexture;
uniform float blurSize;

out vec4 outputFragColor;

void main()
{
	vec4 sum = vec4(0.0);
	
	// blur in y (vertical)
	// take nine samples, with the distance blurSize between them
	sum += texture(renderTexture, vec2(fragTexCoord.x, fragTexCoord.y - 4.0*blurSize)) * 0.05;
	sum += texture(renderTexture, vec2(fragTexCoord.x, fragTexCoord.y - 3.0*blurSize)) * 0.09;
	sum += texture(renderTexture, vec2(fragTexCoord.x, fragTexCoord.y - 2.0*blurSize)) * 0.12;
	sum += texture(renderTexture, vec2(fragTexCoord.x, fragTexCoord.y - blurSize)) * 0.15;
	sum += texture(renderTexture, fragTexCoord) * 0.16;
	sum += texture(renderTexture, vec2(fragTexCoord.x, fragTexCoord.y + blurSize)) * 0.15;
	sum += texture(renderTexture, vec2(fragTexCoord.x, fragTexCoord.y + 2.0*blurSize)) * 0.12;
	sum += texture(renderTexture, vec2(fragTexCoord.x, fragTexCoord.y + 3.0*blurSize)) * 0.09;
	sum += texture(renderTexture, vec2(fragTexCoord.x, fragTexCoord.y + 4.0*blurSize)) * 0.05;
    
//	vec4 orignal = texture(renderTexture, fragTexCoord);
//	outputFragColor = max(orignal, sum);
	
	outputFragColor = sum;
}
