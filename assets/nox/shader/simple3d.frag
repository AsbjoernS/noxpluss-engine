//? required 130

in vec3 outVertex;
in vec3 outNormal;
in vec3 outTangent;
in vec3 outColor;
in vec2 outUv;

uniform vec3 lightPos;
uniform vec3 cameraPos;

uniform sampler2D texture0;


void main() 
{	
	gl_FragColor = vec4(outNormal, 1.0f);
}