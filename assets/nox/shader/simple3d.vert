//? required 130

in vec3 vertex;
in vec3 normal;
in vec3 tangent;
in vec3 color;
in vec2 uv;

uniform mat4 mvpMatrix;
uniform vec3 lightPos;
uniform vec3 cameraPos;

// Send this to fragment shader:
out vec3 outVertex;
out vec3 outNormal;
out vec3 outTangent;
out vec3 outColor;
out vec2 outUv;


void main()
{
	gl_Position = mvpMatrix * vec4(vertex, 1.0);
	
	outNormal = normal;
	outTangent = tangent;
	outColor = color;
	outUv = uv;
}

// https://www.youtube.com/watch?v=ClqnhYAYtcY  (1:18:00)