//? required 130

in vec3 vertex;

uniform mat4 mvpMatrix;

void main()
{
	gl_Position = mvpMatrix * vec4(vertex, 1.0);
	//gl_Position = vec4(vertex, 1.0);

}